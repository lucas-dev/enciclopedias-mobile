function SanitizeHTML(html) {
	var htmlElement = document.createElement("div");
	htmlElement.innerHTML = html;

	var innerLink = function(html) {
	    var arrayMatch = html.match(/@l(.*?)?@l/g);
	    if (arrayMatch) {
		    for (var i=0; i<arrayMatch.length; i++) {
		        var string = arrayMatch[i].split("^^^");
		        var title = string[0].replace("@l","");
		        var link = string[1].replace("@l","");
		        html = html.replace(arrayMatch[i],'<span class="inner-link" data-id="'+link+'">'+title+'</span>');
		    }    
	    }
	    
	    return html;
	}

	var utils = {
		removeElement : function(element) {
			if (element)
				element.parentNode.removeChild(element);
		}
	}

	this.sanitize = function() {
		// add inner-link
		//htmlElement.innerHTML = innerLink(htmlElement.innerHTML);
		// remove .reference class
		var references = htmlElement.querySelectorAll(".reference");
		for (var i=0; i<references.length; i++) {
			utils.removeElement(references[i]);
		}
		// remove href
		var anchors = htmlElement.querySelectorAll("a");
		for (var i=0; i<anchors.length; i++) {
			//var anchor = (anchors[i].href).split("/").pop();
			//console.log(anchor)
			//anchors[i].removeAttribute("href");
		}

		// remove classes
		var classes = htmlElement.querySelectorAll(".noprint, .noexcerpt, .autonumber, .autonumber, .haudio");
		for (var i=0; i<classes.length; i++) {
			classes[i].parentNode.removeChild(classes[i])
		}

		return htmlElement.innerHTML;
	}

}