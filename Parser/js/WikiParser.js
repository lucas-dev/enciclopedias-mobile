function WikiParser() {
	this.options = ["header", "intro", "introStart","introEnd","accordionStart", "accordionEnd", 
					"citation", "imgText", "imgCard1", "imgCard2", 
					"title1","title2","title3", "list1", "list2", "list3Start", 
					"list3End", "list3LI", "list3LIStart", "list3LIEnd",
					"table1", "gallery1", "gallery2", "gallery3", 
					"column2", "column3", "text", "freeScrollStart", "freeScrollEnd"];
	this.optionsPage = ["header", "intro","introStart","accordionStart", "text", "citation", "title1", 
						"title2","title3", "PasteToActive"];

	this.ld = document.querySelector(".ld");
	
	// page area
	this.ldPage = document.querySelector(".ld__page");

	this.iframePage = this.ld.querySelector("iframe");

	// editor area
	this.ldEditor = document.querySelector(".ld__editor");
	
	// preview area
	var ldPreview = document.querySelector(".ld__preview");
	this.iframePreview = ldPreview.querySelector("iframe");

	var pageArea = this.Page();	
	var editorArea = this.Editor();

	// el super objeto
	this.wikiParserObj = {};

	// activando la ordenación de componentes
	$("#sortable").sortable();
	var that = this;
}

WikiParser.prototype.Page = function() {
	var that = this;

	this.btnNewPages = this.ldPage.querySelector(".ld__page__options__new");
	
	// info de paginas
	this.txtCurrentPage = this.ldPage.querySelector(".ld__page__options__navigation__current-page");
	this.txtTotalPages = this.ldPage.querySelector(".ld__page__options__navigation__last-page");
	this.selectPageAddress = this.ldPage.querySelector(".ld__page__options__address");

	// opciones de desplazamiento
	this.btnNavigationPrevious = this.ldPage.querySelector(".ld__page__options__navigation__previous");
	this.btnNavigationNext = this.ldPage.querySelector(".ld__page__options__navigation__next");

	// texto seleccionado
	this.selectedDOM = "";

	// dialogo con las opciones
	this.optionsDialog = this.Utils().createOptionsDialog();
	// cerrar el dialogo cuando se reciba un click fuera de el
	document.body.onclick = function() {
		if (document.contains(that.optionsDialog)) {
			that.Utils().removeElement(that.optionsDialog);
		}
	}
	
	// modal para agregar paginas
	this.ModalNewPages();

	// desplazandose en la lista de paginas
	this.btnNavigationNext.onclick = function() {
		if ((that.wikiParserObj.index) < that.wikiParserObj.pages.length) {
			that.Navigator().moveNext();
		}
	}

	this.btnNavigationPrevious.onclick = function() {
		if (that.wikiParserObj.index > 0) {
			that.Navigator().moveBack();
		}
	}

	// para manejar la selección de texto
	this.iframePage.onload = function() {
		/*
		// para impedir la apertura de hipervinculos
		var links = getIframeWindow().document.querySelectorAll("a");
		for (var i=0; i<links.length;i++) {
			links[i].onclick = function(evt) {
				evt.preventDefault();
			}
		}
		*/
		// redimensionando el contenedor principal con el alto del iframe
		that.ld.style.height = that.Utils().getIframeWindow(that.iframePage).document.body.scrollHeight+"px";
		that.Utils().getIframeWindow(that.iframePage).document.onmouseup = function(evt) {
	  		that.selectedDOM = that.Utils().getSelectionHtml(that.Utils().getIframeWindow(that.iframePage)); 
	  		// si hay texto seleccionado, agregar el popup
	  		if (that.selectedDOM.length) {
		  		that.Utils().attachOptionsDialog(evt.pageX, evt.pageY);
	  		}
		}
	}
}

WikiParser.prototype.Editor = function() {
	var that = this;
	this.lastEditorComponentSelected = null;

	this.btnAddComponent = this.ldEditor.querySelector(".md-add");
	this.btnSyncComponents = this.ldEditor.querySelector(".md-sync");

	this.ldEditorComponents = this.ldEditor.querySelector(".ld__editor__components");

	this.btnAddComponent.onclick = function() {
		that.Utils().insertComponent();
	}

	this.btnSyncComponents.onclick = function() {
		that.Utils().syncComponents();
	}

}

WikiParser.prototype.JSONParser = function() {
	var components = document.querySelectorAll(".ld__editor__components__component");
	var newObj = {};
	newObj.components = [];
	for (var i=0; i<components.length;i++) {
		var component = components[i];
		var selectedOption = component.querySelector("select").value;
		var jsonObj = {};
		jsonObj.type = selectedOption;
		jsonObj.order = i;
		jsonObj.content = {}
		// seleccionamos todos los elementos hijos que tienen data-key
		// para rellenar jsonObj.content con sus valores
		var componentElements = component.querySelectorAll("[data-key]");
		for(var j=0; j<componentElements.length;j++) {
			var element = componentElements[j];
			var key = element.dataset["key"];
			var value = this.Utils().getValueFromElement(element);
			
			// agregamos a jsonObj.content una combinación de key/value
			if (!jsonObj.content[key]) // este if es para setear el valor de los radio
				jsonObj.content[key] = value;
		}
		newObj.components.push(jsonObj);
	}
	this.wikiParserObj.json[this.wikiParserObj.pages[this.wikiParserObj.index]] = newObj;

}

WikiParser.prototype.Navigator = function() {
	var that = this;
	return {
		moveNext: function() {
			that.wikiParserObj.index+=1;
			that.Utils().updatePage();
			that.Utils().fillEditorWithComponents();
			that.Persistence().updateLocalStorage();
			that.Utils().syncComponents();
		},
		moveBack: function() {
			that.wikiParserObj.index-=1;
			that.Utils().updatePage();
			that.Utils().fillEditorWithComponents();
			that.Persistence().updateLocalStorage();
			that.Utils().syncComponents();
		}	
	}	
}

WikiParser.prototype.ModalNewPages = function() {
	var that = this;
	var modalNewPages = document.querySelector(".ld__new-pages");
	var modalNewPagesTextArea = modalNewPages.querySelector("textarea");
	var modalNewPagesButton = modalNewPages.querySelector(".ld__new-pages__btn");
	
	// para mostrar el modal
	this.btnNewPages.onclick = function(evt) {
		evt.stopPropagation();
		that.modalNewPages.style.display = "inline";
	}

	// para conocer la opcion seleccionada del radio
	var selectedOption = 0;
	var radio = modalNewPages.querySelectorAll('input[type="radio"]');
	for (var i=0; i<radio.length;i++) {
		radio[i].onchange = function() {
			var textAreaContent = "";
			if (this.value === "1") {
				// continue parsing
				if (that.Persistence().getLocalStorageObj()) {
					var wikiParserObj = that.Persistence().getLocalStorageObj();
					for (var i=0; i<wikiParserObj.pages.length; i++) {
						textAreaContent += wikiParserObj.pages[i]+"\n";
					}
				} 
			} else {	
				// test
				//textAreaContent = "https://en.wikipedia.org/wiki/Prose_Edda\nhttps://en.wikipedia.org/wiki/Iceland\nhttps://en.wikipedia.org/wiki/Arctic_Circle\nhttps://en.wikipedia.org/wiki/June_solstice";
				textAreaContent = "";
			}
			modalNewPagesTextArea.innerHTML = textAreaContent;
		};
	}

	// boton ACEPTAR del modal
	modalNewPagesButton.onclick = function(evt) {
		if (radio[0].checked) {
			// continue parsing
			that.wikiParserObj = that.Persistence().getLocalStorageObj();
		} else {
			that.Persistence().resetLocalStorage();
			that.wikiParserObj.pages = that.Utils().removeRepeatedItems(modalNewPagesTextArea.value.split("\n"));
			// ordenar alfabeticamente
			that.wikiParserObj.pages.sort();
		}
		that.Utils().updatePage();
		that.Utils().fillEditorWithComponents();
		that.Utils().syncComponents();
		// cerrando el modal
		modalNewPages.style.display = "none";

		// cargando el select con el array de paginas
		for (var i=0; i<that.wikiParserObj.pages.length; i++) {
			var option = document.createElement("option");
			option.text = that.wikiParserObj.pages[i];
			option.value = that.wikiParserObj.pages[i];
			that.selectPageAddress.appendChild(option);
		}
		that.selectPageAddress.value = that.wikiParserObj.pages[that.wikiParserObj.index];
		that.selectPageAddress.onchange = function() {
			var indexByValue = that.wikiParserObj.pages.indexOf(this.value);
			that.wikiParserObj.index = indexByValue;
			that.Utils().updatePage();
			that.Utils().fillEditorWithComponents();
			that.Utils().syncComponents();
		}
	}

	// para cerrar modalNewPages cuando se recibe un click 
	// en el body (no cuentan los iframes)
	document.body.addEventListener("click", function(evt) {
		if (modalNewPages.style.display !== "none")
			modalNewPages.style.display = "none";
	},false);

	modalNewPages.onclick = function(evt) {
		evt.stopPropagation();	
	}

	// seleccionando por defecto el primer radio
	radio[0].click();
}

WikiParser.prototype.Persistence = function() {
	var that = this;
	return {
		resetLocalStorage: function() {
			that.wikiParserObj.index = 0;
			that.wikiParserObj.pages = [];
			that.wikiParserObj.json = {};

			that.Persistence().updateLocalStorage();

		},
		getLocalStorageObj: function() {
			if (localStorage.getItem("WikiParser"))
				return JSON.parse(localStorage.getItem("WikiParser"));
			return null;
		},
		updateLocalStorage: function() {
			localStorage.setItem("WikiParser", JSON.stringify(that.wikiParserObj));
		}
	}
}

WikiParser.prototype.Utils = function() {
	var that = this;
	return {
		syncComponents: function() {
			that.JSONParser();	
			that.Utils().updatePreview();
		},
		updatePage: function() {
			that.selectPageAddress.value = that.wikiParserObj.pages[that.wikiParserObj.index];
			that.txtCurrentPage.innerHTML = that.wikiParserObj.index+1;
			that.txtTotalPages.innerHTML = that.wikiParserObj.pages.length;
			that.iframePage.src = that.wikiParserObj.pages[that.wikiParserObj.index];
		},
		getSelectionHtml: function(windowObj) {
		    var html = "";
		    if (typeof windowObj != "undefined") {
		        var sel = windowObj.getSelection();
		        if (sel.rangeCount) {
		            var container = document.createElement("div");
		            for (var i = 0, len = sel.rangeCount; i < len; ++i) {
		                container.appendChild(sel.getRangeAt(i).cloneContents());
		            }
		            html = container.innerHTML;
		        }
		    } else if (typeof document.selection != "undefined") {
		        if (document.selection.type == "Text") {
		            html = document.selection.createRange().htmlText;
		        }
		    }
		    return html;
		},
		createOptionsDialog: function() {
			var element = document.createElement("div");
			element.className = "ld__modal--elements";

			for (var i=0; i<that.optionsPage.length; i++) {
				var span = document.createElement("span");
				span.innerHTML = that.optionsPage[i];
				(function(span, i) {
					span.onclick = function(evt) {
						evt.preventDefault();
						// cerrar dialogo
						that.Utils().removeElement(element);
						// si se selecciona la ultima opcion, PasteToActive,
						// insertar el texto en el primer textarea vacio
						if (i === that.optionsPage.length-1) {
							if (that.lastEditorComponentSelected) {
								var textAreaToInsertText = that.lastEditorComponentSelected.querySelectorAll("textarea");
								if (textAreaToInsertText) {
									for (var j=0; j<textAreaToInsertText.length; j++) {
										console.log(textAreaToInsertText[j].dataset.key);
										if (!textAreaToInsertText[j].value.length) {
											textAreaToInsertText[j].value = that.selectedDOM;
											break;
										}
									}
								}
							}
						} else {
							var newElementEditor = that.Utils().insertComponent();

							// para que la opcion elegida aparezca 
							// seleccionada en el select del editor
							var select = newElementEditor.querySelector("select");
							select.value = that.optionsPage[i];

							RenderComponent(newElementEditor.querySelector(".ld__editor__components__component__elements"))[that.optionsPage[i]]();
							// para que el dom seleccionado aparezca
							// en el el primer textarea
							var textarea = newElementEditor.querySelector("textarea");
							textarea.innerHTML = that.selectedDOM;
						}


					}
				})(span, i);

				// insertar un espacio cada cuatro elementos
				if (i%4===0) {
					element.appendChild(document.createElement("br"));
				}
				element.appendChild(span);
			}

			return element;
		},
		attachOptionsDialog: function(x, y) {
			if (document.contains(that.optionsDialog)) {
				that.Utils().removeElement(that.optionsDialog);
			}
			document.body.appendChild(that.optionsDialog);
			that.optionsDialog.style.top = (y)+"px";
			that.optionsDialog.style.left = x+"px";
		},
		insertComponent: function() {
			var element = document.createElement("div");
			element.className = "ld__editor__components__component";

			element.onclick = function() {
				if (that.lastEditorComponentSelected)
					that.lastEditorComponentSelected.classList.remove("active-component");
				this.classList.add("active-component");
				that.lastEditorComponentSelected = this;
			}
			element.click();

			// adding select with the editor elements
			var select = document.createElement("select");
			for (var i=0; i<that.options.length; i++) {
				var option = document.createElement("option");
				option.innerHTML = that.options[i];
				select.appendChild(option);
			}

			// adding container of elements
			var container = document.createElement("div");
			container.className = "ld__editor__components__component__elements"
			// populating container
			select.onchange = function() {
				var selectedValue = this.value;
				container.innerHTML = "";
				RenderComponent(container)[selectedValue]();
			}
			
			// adding options area
			var optionArea = document.createElement("div");
			optionArea.className = "ld__editor__components__component__options";
			var optionDelete = document.createElement("span");
			optionDelete.className = "md-delete";
			optionDelete.onclick = function() {
				that.Utils().removeElement(element);
			}

			optionArea.appendChild(optionDelete);
			

			// attaching elements to the container
			element.appendChild(select);
			element.appendChild(container);
			element.appendChild(optionArea);

			// insert element to container
			var li = document.createElement("li");
			li.appendChild(element);
			that.ldEditorComponents.appendChild(li);

			return element;

		},
		removeElement: function(element) {
			element.parentNode.removeChild(element);
		},
		getIframeWindow: function(iframe) {
			return iframe.contentWindow || iframe.contentDocument;
		},
		getElementType: function(element) {
			if(element.tagName && element.tagName.toLowerCase() == "textarea") {
			    return "textarea";
			}

			if(element.tagName && element.tagName.toLowerCase() == "input") {
				if (element.type.toLowerCase() == "text") {
					return "inputText";
				} 

				if (element.type.toLowerCase() == "radio") {
					return "radio";
				} 
			}
		},
		getValueFromElement: function(element) {
			var value = "";
			if (that.Utils().getElementType(element) === "radio") {
				if (element.checked)
					value = element.value;
			} else {
				value = element.value;
			}
			return value;
		},
		getPageId: function() {
			return that.wikiParserObj.pages[that.wikiParserObj.index].split("wiki/")[1];
		}, 
		fillEditorWithComponents: function() {
			that.ldEditorComponents.innerHTML = "";
			if(that.wikiParserObj.json[that.wikiParserObj.pages[that.wikiParserObj.index]]) {
				for (var i=0; i<that.wikiParserObj.json[that.wikiParserObj.pages[that.wikiParserObj.index]].components.length;i++) {
					var componentJSON = that.wikiParserObj.json[that.wikiParserObj.pages[that.wikiParserObj.index]].components[i];

					var newElementEditor = that.Utils().insertComponent();
					// para que el type del json aparezca 
					// seleccionada en el select del editor
					var select = newElementEditor.querySelector("select");
					select.value = componentJSON.type;
					// insertando los componentes del componente
					RenderComponent(newElementEditor.querySelector(".ld__editor__components__component__elements"))[componentJSON.type]();
					// rellenando los componentes del componente con los datos
					// correspondientes del JSON
					var elementsWithKey = newElementEditor.querySelectorAll("[data-key]");
					for (var j=0; j<elementsWithKey.length;j++) {
						var element = elementsWithKey[j];
						// para setear el valor del radio
						if (element.dataset["key"] === "position") {
							if (element.getAttribute("value") === componentJSON.content["position"]) {
								element.setAttribute("checked", "checked");
							} 
						} else {
							// setear el valor del elemento, un textarea por ejemplo, con el valor del json
							element.value = componentJSON.content[element.dataset["key"]];
						}
					}
				}
			}
		},
		updatePreview: function() {
			that.Utils().getIframeWindow(that.iframePreview).reset(new JSONToHTMLConverter(that.wikiParserObj.json[that.wikiParserObj.pages[that.wikiParserObj.index]]).getHTML());
		},
		removeRepeatedItems: function(array) {
			return array.reduce(function(a,b){
			    if (a.indexOf(b) < 0 ) a.push(b);
			    return a;
			  },[])
		}
	}
}