function SimpleSideMenu(menuElement, edgeDirection, animate, animationTime) {
	var that = this;
	this.animationTime = animationTime?animationTime:0.5;
	this.edgeDirection = edgeDirection;
	this.menuElement = menuElement;
	this.coord = {close:-100,open: 0};
	this.menuTransition = {
		enable: function() {
			that.menuElement.style.transition = "transform "+ that.animationTime +"s";
		},
		disable: function() {
			that.menuElement.style.transition = "none";
		}
	}
	this.handleVisibility = 
	{
		close: function() {
			menuElement.style.transform = "translateX("+that.coord.close+"%)";		
		},
		open: function() {
			menuElement.style.transform = "translateX("+that.coord.open+"%)";
		}
	}

	this.wrapper = document.createElement("div");
	this.wrapper.className = "slide-wrapper";
	this.wrapper.appendChild(menuElement);

	this.menuTransition.enable();
	if (edgeDirection === "right") {
		menuElement.style.right = 0;
		this.coord.close = 100;
		this.coord.open = 0;
	}
	
	this.handleVisibility.close();

	this.open = function() {
		document.body.appendChild(this.wrapper);
		this.slide().attachListeners();
		if (animate) {
			setTimeout(function() {
				that.handleVisibility.open();
			}, 100)
		}
	}

	this.close = function() {
		this.menuTransition.enable();
		this.handleVisibility.close();
		setTimeout(function() {
			if (document.body.contains(that.wrapper)) {
				document.body.removeChild(that.wrapper);
			}
		}, that.animationTime*1000);
	    this.slide().detachListeners();
	}

}

SimpleSideMenu.prototype.slide = function() {
	var that = this;
	var mouseDownStart = 0;
	var distanceToMovePx = 0;
	var distanceToMovePercent = 0;
	var canClose = true;
	// para calcular si el desplazamiento es por mouse o por touch
	var axisMovement = function (evt) {
		var eventType = evt.type;
		if (eventType.indexOf("mouse") == 0)
			return evt["pageX"];
		if (eventType.indexOf("touch") == 0)
			return evt.touches[0]["clientX"];
	};

	// eventListeners
	var mouseDown = function(evt) {
	    mouseDownStart = axisMovement(evt);
	    that.menuTransition.disable();
	    canClose = true;
	};

	var mouseMove = function(evt) {
	    if (mouseDownStart > 0) {
	    	canClose = false;
	        distanceToMovePx = axisMovement(evt) - mouseDownStart;
	        distanceToMovePercent = (distanceToMovePx/window.innerWidth)*100;
	        var condition = (that.edgeDirection==="right" && distanceToMovePercent >= that.coord.open)||
	        (that.edgeDirection==="left" && distanceToMovePercent <= that.coord.open);
	        if (condition)
	        	that.menuElement.style.transform = "translateX("+(distanceToMovePercent)+"%)";
	    }
	    
	};

	var mouseUp = function(evt) {
	    mouseDownStart = 0;
	    if (!canClose)
	    	that.close();
	};	
	
	return {
		attachListeners : function() {
			that.wrapper.addEventListener("mousedown", mouseDown, false);
			that.wrapper.addEventListener("touchstart", mouseDown, false);
			that.wrapper.addEventListener("mousemove", mouseMove, false);
			that.wrapper.addEventListener("touchmove", mouseMove, false);
			that.wrapper.addEventListener("mouseup", mouseUp, false);
			that.wrapper.addEventListener("touchend", mouseUp, false);
		},
		detachListeners : function() {
			that.wrapper.removeEventListener("mousedown", mouseDown, false);
			that.wrapper.removeEventListener("touchstart", mouseDown, false);
			that.wrapper.removeEventListener("mousemove", mouseMove, false);
			that.wrapper.removeEventListener("touchmove", mouseMove, false);
			that.wrapper.removeEventListener("mouseup", mouseUp, false);
			that.wrapper.removeEventListener("touchend", mouseUp, false);	
			
		}
	}
}