function RenderComponent(container) {
	var appendTextArea = function(placeholder, key) {
		var textarea = document.createElement("textarea");
		textarea.placeholder = placeholder;
		textarea.dataset.key = key;
		container.appendChild(textarea);
	}
	return {
		header: function() {
			appendTextArea("enter title", "title");
		},
		intro: function() {
			appendTextArea("enter text", "text");
		},
		introStart: function() {
			appendTextArea("enter text", "text");
		},
		introEnd: function() {
		},
		accordionStart: function() {
			appendTextArea("enter title", "title");
		},
		accordionEnd: function() {
			
		},
		citation: function() {
			appendTextArea("enter text", "text");
			appendTextArea("footer", "footer");
		},
		imgText: function() {
			// posicion de la imagen
			var radioLeft = document.createElement("input");
			radioLeft.setAttribute("type", "radio");
			radioLeft.setAttribute("name", "position");
			radioLeft.setAttribute("value", "left");
			radioLeft.setAttribute("checked", "checked");
			radioLeft.dataset.key = "position";
			var spanLeft = document.createElement("span");
			spanLeft.innerHTML = "left";

			var radioRight = document.createElement("input");
			radioRight.setAttribute("type", "radio");
			radioRight.setAttribute("name", "position");
			radioRight.setAttribute("value", "right");
			radioRight.dataset.key = "position";
			var spanRight = document.createElement("span");
			spanRight.innerHTML = "right";

			var radioContainer = document.createElement("div");
			radioContainer.appendChild(radioLeft);
			radioContainer.appendChild(spanLeft);
			radioContainer.appendChild(radioRight);
			radioContainer.appendChild(spanRight);

			appendTextArea("image", "imgSrc");
			appendTextArea("enter text", "text");
			container.appendChild(radioContainer);
		},
		imgCard1: function() {
			appendTextArea("image", "image");
		},
		imgCard2: function() {
			appendTextArea("image", "image");
		},
		title1: function() {
			appendTextArea("enter title", "title");
		},
		title2: function() {
			appendTextArea("enter title", "title");
		},
		title3: function() {
			appendTextArea("enter title", "title");
		},
		list1: function() {
			appendTextArea("enter li", "list");
		},
		list2: function() {
			appendTextArea("enter li", "list");
		},
		list3Start: function() {
			
		},
		list3End: function() {
			
		},
		list3LI: function() {
			appendTextArea("enter title", "title");
		},
		list3LIStart: function() {
			appendTextArea("enter title", "title");
		},
		list3LIEnd: function() {
			
		},
		table1: function() {
			appendTextArea("enter head", "head");
			appendTextArea("enter body", "body");
		},
		gallery1: function() {
			appendTextArea("enter images", "images");
		},
		gallery2: function() {
			appendTextArea("enter images", "images");
		},
		gallery3: function() {
			appendTextArea("enter images", "images");
		},
		column2: function() {
			appendTextArea("enter columns", "column");
		},
		column3: function() {
			appendTextArea("enter columns", "column");
		},
		text: function() {
			appendTextArea("enter text", "html");
		},
		freeScrollStart: function() {

		},
		freeScrollEnd: function() {

		}
	}

}





