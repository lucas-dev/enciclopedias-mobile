function Wiki() {
	var that = this;

	this.currentPage = "";
	this.pages = "";
	// loading splash
	this.ui = new this.UI(this);
	this.ux = new this.UX(this);
	this.navigation = new this.Navigation(this);

	this.dom = new this.DOM();

	this.transitor = new PageTransition(this.dom.container);

	this.vScroll = new VerticalScrollListener(function(scroll) {
		that.ux.headerCallback(scroll);
	});

	this.progressIndicator = new ProgressIndicator();

	this.slideMenu = null;

	this.nativeBridge = new NativeBridge();

	this.hcStuff = new HardCodedStuff();
	this.transitor.init(this.ui.splash());

	this.accordion = new SimpleAccordion(function(i) {
		that.ux.setUpAccordion(i);
	}, false);

	this.galleryFullScreen = null;

	this.searchPlugin = new SearchPlugin(this);
	this.favsPlugin = new FavsPlugin(this);
	this.toast = new SimpleToast();

}

Wiki.prototype.UI = function(that){
	this.splash = function() {
		var div = document.createElement("div");
		div.innerHTML = that.hcStuff.pages.splash();
		
		that.hcStuff.GLOBAL.IMAGES_FOLDER_PATH = that.nativeBridge.getIMGFolderPath();

		that.nativeBridge.imagesScanner();

		return div;
		
	},
	this.landing = function() {
		var div = document.createElement("div");
		div.innerHTML = that.hcStuff.pages.landing();

		return div;
	},
	this.search = function() {
		var div = document.createElement("div");
		div.innerHTML = that.searchPlugin.ux.getDOM();
		return div;	
	},
	this.favs = function() {
		var div = document.createElement("div");
		div.innerHTML = that.favsPlugin.ux.getDOM();
		return div;
	},
	this.about = function() {
		var div = document.createElement("div");
		div.innerHTML = that.hcStuff.pages.about();
		return div;
	},
	// retorna una página, buscando primero en el JSON, sino en las funciones de Wiki.UI
	this.getPage = function(pageId) {
		if (that.pages[pageId]) {
			return that.utils.stringToDOMElement(new JSONToHTMLConverter(that.pages[pageId]).getHTML(that.pages));
		} else if (that.hcStuff.pagesURLMapping[pageId]) {
			return that.utils.stringToDOMElement(new JSONToHTMLConverter(that.pages[that.hcStuff.pagesURLMapping[pageId]]).getHTML(that.pages));
		} else if (that.ui[pageId]) {
			return that.ui[pageId]();
		}
	}
}

Wiki.prototype.UX = function(that) {
	this.activateListeners = function() {
		if (that.dom.dataLink) {
			for (var i=0; i<that.dom.dataLink.length; i++) {
				(function(i) {
					that.dom.dataLink[i].addEventListener("click", function() {
						that.navigation.moveNext(that.dom.dataLink[i].dataset.link);
					});
				})(i);
			}
		}


		if (that.dom.dataURL) {
			var elements = [];
			for (var i=0; i<that.dom.dataURL.length;i++) {
				var imgObj = that.images[that.dom.dataURL[i].dataset.url];
				elements.push({
					image: that.utils.getIMGPath(that, imgObj.md5), 
					title: imgObj.title,
					description1: imgObj.description,
					description2: imgObj.link
				});

				(function(i) {
					that.dom.dataURL[i].onclick = function() {
						that.galleryFullScreen.open(i);
					}
				})(i);
			}

			if (elements)
				that.galleryFullScreen = new GalleryFullScreen(elements, {
					info: function(index) {
						that.nativeBridge.openURL(elements[index].description2);
					},
					imageViewer: function(index) {
						that.nativeBridge.openImageViewer(elements[index].image);
					},
					share: function(index) {
						that.nativeBridge.shareImageAndText(elements[index].image, 
							elements[index].description2);
					}

				});
			else 
				that.galleryFullScreen = null;

		}
		

		var backButtons = that.dom.backButtons();
		if (backButtons) {
			for (var i=0; i<backButtons.length; i++) {
				backButtons[i].onclick = function() {
					that.navigation.moveBack();
				}
			}
		}

		var favIcons = that.dom.favIcons();
		if (favIcons) {
			for (var i=0; i<favIcons.length; i++) {
				favIcons[i].onclick = function() {
					that.ux.favs.handleClick();
				}
			}
		}

		var homeButtons = that.dom.homeButtons();
		if (homeButtons) {
			for (var i=0; i<homeButtons.length; i++) {
				homeButtons[i].onclick = function() {
					that.transitor.navHistory = ["landing","popme"];
					that.navigation.moveBack();
				}
			}
		}

		if (that.dom.listPopupElements) {
			for (var i=0; i<that.dom.listPopupElements.length;i++) {
				(function(i) {
					new SimpleListPopup(that.dom.listPopupElements[i], true, 
						function(callbackIndex) {
							that.ux.setUpListPopup(i, callbackIndex);
						});
				})(i);
			}
		}

	},
	this.processHeader = function() {
		var randomNumber = Math.floor(Math.random() * (9 - 0 + 1)) + 0;

		
		if (that.dom.headerImg) 
			that.dom.headerImg.src = that.utils.getIMGPath(that, that.hcStuff.randomImages[randomNumber]);

		// si no hay datos para el menu de navegación, ocultamos el icono
		if (that.dom.accordions.length) {
			that.ux.sideMenu();
		} else {
			var sideMenuIcons = that.dom.sideMenuElements();
			for (var i=0; i<sideMenuIcons.length; i++) {
				sideMenuIcons[i].style.display = "none";
			}
		}

		that.ux.favs.setUpFavIcons();
	},
	this.headerCallback = function(scrollObj) {
		that.ux.parallaxingHeaderBKG(scrollObj.y/5);
		that.ux.headerOptNav(scrollObj);
	},
	this.sideMenu = function() {
		var navMenu = document.createElement("div");
		navMenu.className = "slide-menu ld__menu";

		for (var i=0; i<that.dom.accordions.length;i++) {
			var accordionHeader = that.dom.accordionTitles(that.dom.accordions[i]);

			// append accordion headers
			var divHeader = document.createElement("div");
			divHeader.innerHTML = '<div class="ld__menu__title">'+accordionHeader.innerHTML+'</div>';
			(function(accordionHeader) {
				divHeader.onclick = function() {
					scrollToTitle(accordionHeader, null);
				}
			})(accordionHeader);
			navMenu.appendChild(divHeader);

			// append titles
			var titles = that.dom.accordionTitles1(that.dom.accordions[i]);
			for (var k=0; k<titles.length;k++) {
				var divTitle = document.createElement("div");
				divTitle.innerHTML = '<div class="ld__menu__subtitle">'+titles[k].innerHTML+'</div>';
				(function(k, titles, i) {
					divTitle.onclick = function() {
						scrollToTitle(titles[k], i);
					}
				})(k, titles, i);
				navMenu.appendChild(divTitle);						
			}
		}
		that.slideMenu = new SimpleSideMenu(navMenu, "right", true, .5);
		
		var sideMenuIcons = that.dom.sideMenuElements();
			for ( var i=0;i<sideMenuIcons.length;i++ ) {
				sideMenuIcons[i].onclick = function() {
					that.slideMenu.open();
				}
			}

		var scrollToTitle = function(title, headerParentIndex) {
			if (title.getBoundingClientRect().top == 0 && headerParentIndex!=null) {
				that.accordion.activateAccordion(headerParentIndex);
			}
			that.ux.scrollToY((title.getBoundingClientRect().top+that.dom.body.scrollTop), 1500, "easeInOutQuint");
		}
	},
	this.addImages = function() {
		if (that.dom.dataURL) {
			for (var i=0; i<that.dom.dataURL.length;i++) {
				var img = that.dom.img(that.dom.dataURL[i]);
				if (!that.images[that.dom.dataURL[i].dataset.url]) {
					console.log("image not found: "+that.dom.dataURL[i].dataset.url);
				} else {
					var imgPath = that.images[that.dom.dataURL[i].dataset.url].md5;
					img.src = that.utils.getIMGPath(that, imgPath);
				}
			}
		}
	},
	// parallax a la imagen del header
	this.parallaxingHeaderBKG = function(y) {
		if ( (that.dom.headerImg) && (that.dom.ldHeader.getBoundingClientRect().bottom > 0 || y===0))
			that.dom.headerImg.style["-webkit-transform"] = 'translate3d(0px,' + y + 'px, 0px)';
			
	}, 
	this.headerOptNavAppendToBody = function() {
		// agregamos un nuevo fixed header al documento, si existe un headerOpt original
		if (that.dom.headerOpt) {
			that.dom.headerOptCopy.classList.add("ld__header__options--fixed");
			that.dom.body.appendChild(that.dom.headerOptCopy);
		}
	},
	// mostrar la copia del header de opciones solo cuando se scrolea hacia arriba
	this.headerOptNav = function(scrollObj) {
		// si la pagina tiene headerOpt, hacer esto del scroll
		if (that.dom.headerOpt) {
			if ( scrollObj.direction === scrollObj.UP ) {
				// hacemos visible la copia del header
				// siempre que el titulo no aparezca por completo
				if ( that.dom.headerTitle.getBoundingClientRect().top <= 0 ) {
					that.dom.headerOptCopy.style.visibility = "visible";
					that.dom.optTitleCopy.style.visibility = "visible";
				} else {
					that.dom.headerOptCopy.style.visibility = "hidden";
					that.dom.optTitleCopy.style.visibility = "hidden";
				}
			} else {
				// si se hace un scroll hacia abajo, ocultar la copia del header y el titulo
				// de la copia
				that.dom.headerOptCopy.style.visibility = "hidden";
				that.dom.optTitleCopy.style.visibility = "hidden";
			}
		}
	}

	this.setUpAccordion = function(i) {
		var icon = that.dom.accordionIcon(i);
		if (icon.classList.contains("md-expand-more")) {
			icon.classList.remove("md-expand-more");
			icon.classList.add("md-expand-less");
		} else {
			icon.classList.add("md-expand-more");
			icon.classList.remove("md-expand-less");
		}
	}

	this.setUpListPopup = function(index, callbackIndex) {
		var expandBtn = that.dom.getExpandMoreIcon(index, callbackIndex);
		if (expandBtn) {
			if (expandBtn.classList.contains("rotate0to180")) {
				expandBtn.classList.add("rotate180to0");
				expandBtn.classList.remove("rotate0to180");
			} else {
				expandBtn.classList.add("rotate0to180");
				expandBtn.classList.remove("rotate180to0");
			}

		}
	}
	// https://github.com/jbraithwaite/scroll-to-y/blob/master/scroll-to-y.js
	this.scrollToY = function(scrollTargetY, speed, easing) {
	    var scrollY = window.scrollY,
	        scrollTargetY = scrollTargetY || 0,
	        speed = speed || 2000,
	        easing = easing || 'easeOutSine',
	        currentTime = 0;

	    var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));

	    var PI_D2 = Math.PI / 2,
	        easingEquations = {
	            easeOutSine: function (pos) {
	                return Math.sin(pos * (Math.PI / 2));
	            },
	            easeInOutSine: function (pos) {
	                return (-0.5 * (Math.cos(Math.PI * pos) - 1));
	            },
	            easeInOutQuint: function (pos) {
	                if ((pos /= 0.5) < 1) {
	                    return 0.5 * Math.pow(pos, 5);
	                }
	                return 0.5 * (Math.pow((pos - 2), 5) + 2);
	            }
	        };

	    function tick() {
	        currentTime += 1 / 60;

	        var p = currentTime / time;
	        var t = easingEquations[easing](p);

	        if (p < 1) {
	            requestAnimationFrame(tick);

	            window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
	        } else {
	            window.scrollTo(0, scrollTargetY);
	        }
	    }

	    tick();
	},
	this.favs= {
		setUpClasses: function(icons, enableFavIcon) {
			for (var i=0; i<icons.length; i++) {
				if (enableFavIcon) {
					icons[i].classList.remove("md-star-outline");
					icons[i].classList.add("md-star");
				} else {
					icons[i].classList.remove("md-star");
					icons[i].classList.add("md-star-outline");
				}
			}	
		},
		setUpFavIcons: function() {
			var favIcons = that.dom.favIcons();
			if (favIcons && favIcons.length) {
				that.ux.favs.setUpClasses(favIcons, that.favsPlugin.utils.isPageInFavsList(that.currentPage));
			}
		},
		handleClick: function() {
			var favIcons = that.dom.favIcons();
			var isFav = that.favsPlugin.utils.isPageInFavsList(that.currentPage);
			that.ux.favs.setUpClasses(favIcons, !isFav);
			if (isFav) {
				that.favsPlugin.persistence.removePage(that.currentPage);
				that.toast.open("Removed from Favs");
			} else {
				that.favsPlugin.persistence.savePage(that.currentPage);
				that.toast.open("Added to Favs");
			}
		}	
	},
	this.splashMoveNext = function() {
		that.utils.ajaxRequest("json/pages.json", function(data) {
			that.pages = JSON.parse(data);
			that.ux.splashUpdateStatusLabel("Loading images...");
			that.utils.ajaxRequest("json/images.json", function(images) {
				that.images = JSON.parse(images);
				that.ux.splashUpdateStatusLabel("All data loaded");
				setTimeout(function() {
					that.ux.splashUpdateStatusLabel("");
				}, 500);

				console.log("Pages: "+Object.keys(that.pages).length)
				console.log("Images: "+Object.keys(that.images).length)

				that.progressIndicator.stop();
				setTimeout(function() {
					that.navigation.moveNext("landing", true);
				}, 3000);
			});
		});
	},
	this.splashUpdateStatusLabel = function(text) {
		if (!that.dom.splashStatusLabel)
			that.dom.initSplashStatusLabel();
		that.dom.splashStatusLabel.innerHTML = text;
	},
	this.doAnnoyingCarouselSetup = function() {
		if (that.dom.carouselGalleryItems) {
			// para saber la cantidad de cards en los accordions
			var cardsLength = 0;
			for (var i=0; i<that.dom.carouselGalleryItems.length; i++) {
				cardsLength+= that.dom.carouselGalleryItems[i].children[0].children.length;
			}			

			// por cada imagen de cada card de los accordion, esperar a que carguen todo
			// para definir el ancho del texto
			for (var i=0; i<that.dom.carouselGalleryItems.length; i++) {
				var c = 0;
				var cards = that.dom.carouselGalleryItems[i].children[0].children;
				for (var j=0; j<cards.length;j++) {
			        cards[j].children[0].onload = function() {
			            this.parentNode.children[1].style.width = this.offsetWidth+"px";
			            c++;
			            // cuando se procesaron todas las imagenes, habiliar los carouseles
			            // y despues, colapsar los acordeones;
			            // ya que el colapso hace un display: none, que si se ejecuta antes 
			            // de que las imagenes esten procesadas, no se van a poder calcular
			            // las dimensiones de los carousel.
			            if (c===cardsLength) {
			            	for (var i=0; i<that.dom.carouselGalleryItems.length; i++) {
								new HCarousel(that.dom.carouselGalleryItems[i], null, null);
							}
			            	that.accordion.collapseAll();
			            }
			        }
			    }
			}
		}
	}
}


Wiki.prototype.DOM = function() {
	// referencias estaticas
	this.body = document.body;
	this.container = document.querySelector(".ld");


	// referencias no estaticas
	this.page = null;
	
	this.ldHeader = null;
	this.headerOpt = null;
	this.headerOptCopy = null;
	this.optTitleCopy = null;
	this.headerImg = null;
	this.headerTitle = null;
	this.dataLink = null;
	this.accordions = null;
	this.dataURL = null;
	this.carouselGalleryItems = null;
	this.listPopupElements = null;
	this.splashStatusLabel = null;

	this.accordionTitles = function(accordion) {
		return accordion.querySelector(".ld__accordion__header__title");
	}

	this.accordionTitles1 = function(accordion) {
		return accordion.querySelectorAll(".ld__title--1");
	}

	this.img = function(element) {
		return element.querySelector("img");
	}

	// sobre el body hacemos el query para abarcar el header fixed
	this.sideMenuElements = function() {	
		return this.body.querySelectorAll(".md-storage");
	}

	this.backButtons = function() {
		return this.body.querySelectorAll(".md-chevron-left")
	}

	this.favIcons = function() {
		return this.body.querySelectorAll(".fav-icon");
	}

	this.homeButtons = function() {
		return this.body.querySelectorAll(".md-home");
	}

	this.getExpandMoreIcon = function(index, callbackIndex) {
		return this.listPopupElements[index].children[callbackIndex].querySelector(".md-expand-more");
	}

	this.initSplashStatusLabel = function() {
		this.splashStatusLabel = this.body.querySelector(".ld__splash__info");
	}

	this.reloadPageElements = function(page) {
		this.page = page;
		this.ldHeader = this.page.querySelector(".ld__header");
		this.headerOpt = this.page.querySelector(".ld__header__options");
		this.headerImg = this.page.querySelector(".ld__header__img");
		this.headerTitle = this.page.querySelector(".ld__header__title");
		this.dataLink = this.page.querySelectorAll("[data-link]");
		this.accordions = this.page.querySelectorAll(".ld__accordion");
		this.dataURL = this.page.querySelectorAll("[data-url]");
		this.carouselGalleryItems = this.page.querySelectorAll(".ld-hcarousel");
		this.listPopupElements = this.page.querySelectorAll(".list-popup");

		// removemos el headerOptCopy del body, en caso de que sea un child		
		if (this.headerOptCopy) {
			this.headerOptCopy.parentNode.removeChild(this.headerOptCopy)
		}
		if (this.headerOpt) {
			this.headerOptCopy = this.headerOpt.cloneNode(true);
			this.optTitleCopy = this.headerOptCopy.querySelector(".ld__header__options__title");
		} else {
			this.headerOptCopy = null;
		}
	}

	this.reloadElement = function(key, value) {
		return this.key.value;
	}


	this.accordionIcon = function(i) {
		return this.page.getElementsByClassName("accordion")[i].querySelector(".ld__accordion__header__expand");
	}
}


Wiki.prototype.Navigation = function(that) {
	this.moveNext = function(pageId, disableTransition) {
		if (that.transitor.transitionFinished) {
			console.log("NEXT: "+pageId);
			that.currentPage = pageId;
			that.progressIndicator.start();
			that.ux.parallaxingHeaderBKG(0);
			var page = null;
			setTimeout(function() {
				page = that.ui.getPage(pageId);
				if (page) {
					that.transitor.moveNext(page, pageId, function() {
						that.dom.reloadPageElements(page);

						that.ux.headerOptNavAppendToBody();

						that.ux.processHeader();
						that.ux.doAnnoyingCarouselSetup();
						that.ux.addImages();

						that.accordion.init(page);

						if (!that.dom.carouselGalleryItems.length)
							that.accordion.collapseAll();

						setTimeout(function() {
							that.ux.activateListeners();
							that.progressIndicator.stop();
						}, 0);

						if (pageId === "search") {
							that.searchPlugin.init();
							that.searchPlugin.searchCallback = function(link) {
								that.navigation.moveNext(link);
							}
						}

						if (pageId === "favs") {
							that.favsPlugin.ux.reloadDOM();
							that.favsPlugin.ux.populateFavsArea();
							that.favsPlugin.ux.setUpDataLink();
							that.favsPlugin.favsCallback = function(link) {
								that.navigation.moveNext(link);
							}
						}
					}, disableTransition);
				}
			}, 0);
		}
	}

	this.moveBack = function() {
		if (that.transitor.transitionFinished) {
			if (that.galleryFullScreen.ux.isGalleryActive()) {
				that.galleryFullScreen.ux.close();
			} else {
				if (that.slideMenu && that.slideMenu.isMenuActive()) {
					that.slideMenu.close();
				}

				var page = that.transitor.moveBack(function(navId) {
					console.log("BACK: "+navId);
					that.currentPage = navId;
					return that.ui.getPage(navId);
				});
				if (page) {
					that.progressIndicator.start();
					that.ux.parallaxingHeaderBKG(0);
					setTimeout(function() {
						that.dom.reloadPageElements(page);

						that.ux.headerOptNavAppendToBody();

						that.ux.processHeader();
						that.ux.doAnnoyingCarouselSetup();
						that.ux.addImages();

						that.accordion.init(page);

						setTimeout(function() {
							that.ux.activateListeners();
							that.progressIndicator.stop();
						}, 0);

						if (that.currentPage === "search")
							that.searchPlugin.ux.reloadBack();

						if (that.currentPage === "favs") {
							that.favsPlugin.ux.reloadDOM();
							that.favsPlugin.ux.populateFavsArea();
							that.favsPlugin.ux.setUpDataLink();
							that.favsPlugin.favsCallback = function(link) {
								that.navigation.moveNext(link);
							}
						}
					}, 0);
				}
			}
		}
	}
}

Wiki.prototype.utils = {
	ajaxRequest: function(url, callback) {
					var xmlHTTP= new XMLHttpRequest();
					xmlHTTP.onreadystatechange= function() {
						if(xmlHTTP.readyState == XMLHttpRequest.DONE) {
							// status === 0 para tests en el browser
							if(xmlHTTP.status === 200 ) {
								callback(xmlHTTP.responseText);
							}
						}
					}

					xmlHTTP.open("GET", url, true);
					xmlHTTP.send();
				},
	stringToDOMElement: function(html) {
		var div = document.createElement("div");
		div.innerHTML = html;
		return div.firstChild;
	},
	setOpacityRGBA: function(color, distanceToMove) {
		if ( color.toLowerCase().indexOf("rgba")>-1) {
			if (distanceToMove > 0) {
				var lastComma = color.lastIndexOf(',');
				var lastBracket = color.lastIndexOf(')');
				var opacityFromRGBA = parseFloat(color.substring(lastComma+1,lastBracket));
				var opacity = (opacityFromRGBA*(100-distanceToMove))/100;
				return color.slice(0, lastComma + 1) + Math.abs(opacity) + ")";
			}
		} else if ( color.toLowerCase().indexOf("rgba")===-1) {
			var opacity = Math.abs(((distanceToMove * 1) / 100)-1);
			opacity = 1-(opacity);
			opacity = opacity*8;
			return color.replace(")",","+Math.abs(opacity)+")").replace("rgb","rgba");
		}
	},
	getIMGPath: function(wikiObj, url) {
		return wikiObj.hcStuff.getIMGFolderPath()+url+".jpg";
	}
}