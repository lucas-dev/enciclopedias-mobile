function NativeBridge() {
	this.shareImageAndText = function(imagePath, text) {
		Android.shareImageAndText(imagePath, text);
	}

	this.openImageViewer = function(path) {
		Android.openImageViewer(path);
	}

	this.openURL = function(url) {
		Android.openURL(url);
	}

	this.imagesScanner = function() {
		Android.imagesScanner();
	}

	this.getIMGFolderPath = function() {
		return Android.getIMGFolderPath();
	}
}