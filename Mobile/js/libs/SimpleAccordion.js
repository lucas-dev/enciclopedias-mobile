function SimpleAccordion(callback, closeLastActive) {
    var that = this;
    var lastActive = null;
    var accordionsObj = null;
    var lastActive = 0;
    this.activateAccordion = function (i) {
        if (closeLastActive && lastActive)
            accordionsObj[lastActive].content.style.display = "none";

        if (accordionsObj[i].content) {
            if (accordionsObj[i].content.style.display === "none") {
                accordionsObj[i].content.style.display = "block";
            } else {
                accordionsObj[i].content.style.display = "none";
            }
            lastActive = i;
        }
        callback(i);
    }

    this.init = function (parent) {
        var accordionsDOM = parent.querySelectorAll(".accordion");
        lastActive = 0;
        accordionsObj = [];
        for (var i=0; i<accordionsDOM.length; i++) {
            accordionsObj.push({
                container: accordionsDOM[i],
                header: accordionsDOM[i].querySelector(".accordion__header"),
                content: accordionsDOM[i].querySelector(".accordion__content"),
                closeButton: accordionsDOM[i].querySelector(".accordion__close")
            });
            (function(i) {
                accordionsObj[i].header.onclick = function() {
                    that.activateAccordion(i);
                }

                if (accordionsObj[i].closeButton)
                    accordionsObj[i].closeButton.onclick = function () {
                        that.activateAccordion(i);
                    }
            })(i);
        }
    }

    this.collapseAll = function() {
        for (var i=0; i<accordionsObj.length; i++) {
            accordionsObj[i].content.style.display = "none";
        }
    }

    this.openAll = function() {
        for (var i=0; i<accordionsObj.length; i++) {
            accordionsObj[i].content.style.display = "block";
        }   
    }
}