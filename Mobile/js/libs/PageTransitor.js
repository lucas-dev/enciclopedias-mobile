// TODO
	// que transitionTime sea parametrizado
	// normalizar todo lo que figura como -webkit
	// deshabilitar temporalmente el scroll antes de cada transicion

function PageTransition(parent) {
	var that = this;
	var childToDelete = null;

	this.transitionFinished = true;

	var container = document.createElement("div");
	container.style.overflow = "hidden";
	container.style.width = "200%";
	container.style.position = "relative";

	container.addEventListener( 'webkitTransitionEnd',  function(event) { 
		if (childToDelete!=null) {
			container.removeChild(container.children[childToDelete]);	
			moveContainer(0,0);
			childToDelete = null;
			that.transitionFinished = true;
		}
	}, false );

	parent.style.position = "relative";
	parent.style.overflow = "hidden";
	parent.appendChild(container);

	this.navHistory = [];

	var stylePage = function(page) {
		page.style.float = "left";
		page.style.width = "50%";
		page.style.position = "relative";
		page.style.overflowY = "auto";
	}

	var moveContainer = function(transitionTime, translationVal) {
		if (transitionTime === 0) {
			container.style["-webkit-transition-property"] = "none";
			container.style["-webkit-transform"] = "translate3d("+translationVal+"px,0,0)";
		} else {
			container.style["-webkit-transition"]= "-webkit-transform "+transitionTime+"s";
			container.style["-webkit-transform"] = "translate3d("+(-Math.abs(translationVal))+"px,0,0)";
		}
	}

	this.init = function(page) {
		stylePage(page);
		container.appendChild(page);
	}

	this.moveNext = function(page, id, callback, disableTransition) {
		this.transitionFinished = disableTransition?true:false;
		// para que mantenga la ultima pos del scroll
		// container.children[0].style.top = -Math.abs(window.scrollY)+"px"; 
		this.navHistory.push(id);
		stylePage(page);
		container.appendChild(page);
		
		callback();

		setTimeout(function() {
			document.body.scrollTop = document.documentElement.scrollTop = 0;
			moveContainer(disableTransition?0.001:0.4, page.offsetWidth);
			childToDelete = 0;
		}, 100); //give some rest to the CPU
		
	}


	this.moveBack = function(callbackPage) {
		if (that.navHistory.length>1) {
			this.transitionFinished = false;
			this.navHistory.pop();
			var page = callbackPage(this.navHistory[this.navHistory.length-1]);
			if (page) {
				stylePage(page);
				container.insertBefore(page, container.children[0]);
				moveContainer(0, -Math.abs(page.offsetWidth));
				childToDelete = null;
				
				document.body.scrollTop = document.documentElement.scrollTop = 0;
				moveContainer(0.4, 0); 
				childToDelete = 1;
				return page;
			} 
		}
		this.transitionFinished = true;
		return null;
	}
}