// el container debe tener definido el ancho
function HCarousel(container, onClickCallback, rightSpace) {
    var that = this;
    // es el padding right que va a tener el ultimo elemento
    rightSpace = rightSpace?rightSpace:20;

    this.wrapper = container.querySelector(".ld-hcarousel__wrapper");
    this.cards = container.querySelectorAll(".ld-hcarousel__wrapper__card");

    that.wrapper.style.width = that.wrapper.scrollWidth+"px";

    if (onClickCallback) {
        for (var i=0; i<this.cards.length; i++) {
            (function(i) {
                that.cards[i].addEventListener("click", function() {
                    onClickCallback(i);
                });
            })(i);
        }
    }

    this.distanceToMovePercent = 0;
    this.distanceToMoveBeforeMouseUp = 0;

    // handling horizontal scroll
    var hammer = new Hammer(container);

    hammer.on("panleft panright", function(evt) {
        var leftCloseLimit = 100-((window.innerWidth/(that.wrapper.offsetWidth+rightSpace))*100);
        var rightCloseLimit = 0;

        var leftCloseCondition = Math.abs(that.distanceToMoveBeforeMouseUp + ((evt.deltaX/window.innerWidth)*100))<leftCloseLimit;
        var rightCloseCondition = that.distanceToMoveBeforeMouseUp + ((evt.deltaX/window.innerWidth)*100)<rightCloseLimit;

        if (leftCloseCondition && rightCloseCondition) {
            that.distanceToMovePercent = ((evt.deltaX/window.innerWidth)*100);
            that.wrapper.style["-webkit-transform"] = "translate3d("+(that.distanceToMovePercent+that.distanceToMoveBeforeMouseUp)+"%, 0, 0)";
        } else {
            if (!leftCloseCondition) {
                that.wrapper.style["-webkit-transform"] = "translate3d(-"+(leftCloseLimit)+"%, 0, 0)";
            } 
            if (!rightCloseCondition) {
                that.wrapper.style["-webkit-transform"] = "translate3d("+(rightCloseLimit)+"%, 0, 0)";
            }
        }
    });

    hammer.on("panend", function(evt) {
        that.distanceToMoveBeforeMouseUp += that.distanceToMovePercent;
    });


    /* specific for the wiki app */
    hammer.on("tap", function(evt) {
        if (evt.target.parentNode.className === "ld-hcarousel__wrapper__card") {
            evt.target.parentNode.click();
        }
    });
}