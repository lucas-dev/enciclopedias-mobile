function VerticalScrollListener(callback) {
	var that = this;
	// up (1) - down (0)
	this.direction = null;
	this.UP = 1;
	this.DOWN = 0;

	// posición vertical del scrollbar
	this.y = null;
	// lo que se scrollo en relacion al tamaño del documento
	this.scrollPercentage = null;
	// get number of pixels document has scrolled vertically 
	var scrollTopInitial = window.pageYOffset;
		
	var didScroll = false;

	window.addEventListener("scroll", function(evt) {
		didScroll = true;
	});

	function scrollback() {
	    if ( didScroll ) {
	        didScroll = false;
		  	that.scrollPercentage = 100 * document.body.scrollTop / (document.body.scrollHeight-document.body.clientHeight);
			that.y = window.pageYOffset;
		  	that.direction = that.UP;
		  	if (that.y > scrollTopInitial) {
		  		that.direction = that.DOWN;
		  	}
		  	scrollTopInitial = that.y;
		  	callback(that);
	    }
		requestAnimationFrame(scrollback);
	}

	requestAnimationFrame(scrollback);
	

}	

// requestAnimationFrame polyfill
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());