function HardCodedStuff() {
	var that = this;
	this.GLOBAL = {
		LOCAL_STORAGE_KEY: "NORSE_ENC_FAVS",
		//IMAGES_FOLDER_PATH: "file:///home/lucas/Dropbox/dev/Encyclopedia/WIKI/app/src/main/assets/img/",
		//IMAGES_FOLDER_PATH: "img/",
		IMAGES_FOLDER_PATH: ""
	}

	this.getIMGFolderPath = function() {
		return encodeURI(this.GLOBAL.IMAGES_FOLDER_PATH);
	}


	this.pages = {
		about: function() {
			return '<div class="ld__info-screen">\
						<div class="ld__header__options">\
							<div>\
								<span class="md-chevron-left"></span>\
							</div>\
							<div class="ld__header__options__title">About</div>\
						</div>\
						<div class="ld__header">\
							<img class="ld__header__img">\
							<div class="ld__header__title">About</div>\
						</div>\
						<div class="ld__info-screen__content">\
							<p><b>Norse mythology</b> is the body of mythology of the North Germanic people stemming from Norse paganism and continuing after the Christianization of Scandinavia and into the Scandinavian folklore of the modern period. The northernmost extension of Germanic mythology, Norse mythology consists of tales of various deities, beings, and heroes derived from numerous sources from both before and after the pagan period, including medieval manuscripts, archaeological representations, and folk tradition.</p>\
							<p>The "Norse Mythology Encyclopedia" mobile application provides an interface based on the content available on\
							Wikipedia (www.wikipedia.org) for and quicker and full offline reading experience.</p>\
							<h3>Licence</h3>\
							<p>Written content available on the app has generally been made available by Wikipedia or its licensors under the the Creative Commons Attribution-ShareAlike 4.0 International License ("CC BY-SA") and/or the GNU Free Documentation License ("GFDL"). The app uses all such text pursuant to the terms of the CC BY-SA and you are free to use such content in compliance with such license. Certain specific text may be made available by Wikipedia under other free licenses, and the app makes such text available under the applicable license.\
							Certain data available on the app may be obtained Freebase, pursuant to the Creative Common Attribution Only ("CC-BY") license, and you are free to use such content in compliance with such license. See https://www.freebase.com/ for more details.</p>\
							<p>Images available on the app are generally in the public domain or have been made available on Wikipedia under free licenses or using a "fair use" exception. This app makes best efforts to comply with the applicable licenses, and appreciate any questions or comments you have regarding our compliance policies.</p>\
							<h3>Copyright</h3>\
							<p>The policy of this app is not to infringe or violate the intellectual property rights or other rights of any third party and, if properly informed, Alvefard will remove material appearing on the app that infringes the rights of any third party. Under the Digital Millennium Copyright Act of 1998 (the "DMCA"), Alvefard will remove any content if properly notified of that such material infringes third party rights, and may do so at its sole discretion, without prior notice to users at any time.</p>\
							<p>If you believe that something appearing on the app infringes your copyright, you may send us a notice requesting that it be removed, or access to it blocked. If you believe that such a notice has been wrongly filed against you, the DMCA lets you send us a counter-notice. Notices and counter-notices must meet the DMCA"s requirements. We suggest that you consult your legal advisor before filing a notice or counter-notice. Be aware that there can be substantial penalties for false claims. Send notices and counter-notices to Alvefard by contacting alvefard@gmail.com.</p>\
							<h3>Contact</h3>\
							<p>If you have any feedback, questions, suggestions, comments or complaints about this mobile encyclopedia or any other topic, please contact to <a href="mailto:alverfard@gmail.com?Subject=Norse%20Mythology%20Encyclopedia">alvefard@gmail.com</a>.</p>\
							<p>So please, before giving a bad review, if you find any bug or if you have any idea to improve the application, do not hesitate to contact me so I can start working on it as soon as possible.</p>\
							<p>Thanks in advance!</p>\
						</div>\
					</div>';
		},
		landing: function() {
			return '<div class="ld__header">\
						<img class="ld__header__img">\
						<div class="ld__header__title landing-title">Norse Mythology<br>Encyclopedia</div>\
					</div>\
					<div class="ld__tile">\
						<div class="ld__tile__row">\
							<div class="ld__tile__row__column landing-cosmology margin-right" data-link="Cosmology" style="background-image:url(\''+that.getIMGFolderPath()+'4fbde9ddb0c8b527561334e74f2df6bf.jpg\')"><span>Cosmology</span></div>\
							<div class="ld__tile__row__column landing-gods mright0" data-link="GodsAndOtherBeigns" style="background-image:url(\''+that.getIMGFolderPath()+'7e669e91d8d365ab91ca6e54d4b6461a.jpg\')"><span>Gods and other Beigns</span></div>\
						</div>\
						<div class="ld__tile__row">\
							<div class="ld__tile__row__column landing-worship margin-right" data-link="NorseWorship" style="background-image:url(\''+that.getIMGFolderPath()+'8ac3a4fe0571ae9368006d7d4f7ea85c.jpg\')"><span>Norse Worship</span></div>\
							<div class="ld__tile__row__column landing-events margin-right" data-link="Events" style="background-image:url(\''+that.getIMGFolderPath()+'40a8b547c5b8ea5cd18fd81f4b1fc90b.jpg\')"><span>Events</span></div>\
							<div class="ld__tile__row__column landing-sources mright0" data-link="Sources" style="background-image:url(\''+that.getIMGFolderPath()+'409ab4b8cf70025163e3732d85b8476e.jpg\')"><span>Sources</span></div>\
						</div>\
						<div class="ld__tile__row">\
							<div class="ld__tile__row__column landing-christianity margin-right" data-link="InteractionsWithChristianity" style="background-image:url(\''+that.getIMGFolderPath()+'943fa366bae165db0695c0ef4242aa2b.jpg\')"><span>Interactions with christianity</span></div>\
							<div class="ld__tile__row__column landing-artifacts margin-right mright0" data-link="Artifacts" style="background-image:url(\''+that.getIMGFolderPath()+'5677aeb9630d527aa9c82d1c102d4f4c.jpg\')"><span>Artifacts</span></div>\
						</div>\
						<div class="ld__tile__row">\
							<div class="ld__tile__row__column landing-ships margin-right" data-link="Ships" style="background-image:url(\''+that.getIMGFolderPath()+'6952f9edb36d7725591ebe223c56fd37.jpg\')"><span>Ships</span></div>\
							<div class="ld__tile__row__column landing-locations margin-right mright0" data-link="Locations" style="background-image:url(\''+that.getIMGFolderPath()+'4660e745b2953c82a90a46d6990c23e9.jpg\')"><span>Locations</span></div>\
						</div>\
						<div class="ld__tile__row">\
							<div class="ld__tile__row__column landing-favourites margin-right" data-link="favs" style="background-image:url(\''+that.getIMGFolderPath()+'54803d33d78506611c1b7943e5e34b96.jpg\')"><span>Favourites</span></div>\
							<div class="ld__tile__row__column landing-search margin-right" data-link="search" style="background-image:url(\''+that.getIMGFolderPath()+'a1b80f4a7f59cce8b65d47e54c6056c9.jpg\')"><span>Search</span></div>\
							<div class="ld__tile__row__column landing-about mright0" data-link="about" style="background-image:url(\''+that.getIMGFolderPath()+'fe6f220d870d5bd3acf816026bbaaa26.jpg\')"><span>About</span></div>\
						</div>\
					</div>';
		},
		splash: function() {
			return '<div class="ld__splash" style="background-image:url(\'img/5e4f68bbbadf439261bd05e8b9e2c76e.jpg\')">\
						<div class="ld__splash__title ld__splash__text">\
							Norse Mythology<br>\
							Encyclopedia\
						</div>\
						<div class="ld__splash__info ld__splash__text"></div>\
					</div>';
		}
	}

	this.pagesURLMapping = {
		"https://en.wikipedia.org/wiki/Viking_Runestones":"https://en.wikipedia.org/wiki/Viking_runestones",
		"https://en.wikipedia.org/wiki/Varangian_Runestones":"https://en.wikipedia.org/wiki/Varangian_runestones",
		"https://en.wikipedia.org/wiki/Egils_saga":"https://en.wikipedia.org/wiki/Egil%27s_Saga",
		"https://en.wikipedia.org/wiki/Eir%C3%ADks_saga_rau%C3%B0a":"https://en.wikipedia.org/wiki/Saga_of_Erik_the_Red",
		"https://en.wikipedia.org/wiki/Gr%C5%93nlendinga_saga":"https://en.wikipedia.org/wiki/Greenland_saga",
		"https://en.wikipedia.org/wiki/H%C3%A1var%C3%B0ar_saga_%C3%8Dsfir%C3%B0ings":"https://en.wikipedia.org/wiki/The_saga_of_H%C3%A1var%C3%B0ur_of_%C3%8Dsafj%C3%B6r%C3%B0ur",
		"https://en.wikipedia.org/wiki/Reykd%C5%93la_saga_ok_V%C3%ADga-Sk%C3%BAtu":"https://en.wikipedia.org/wiki/Reykd%C3%A6la_saga_ok_V%C3%ADga-Sk%C3%BAtu",
		"https://en.wikipedia.org/wiki/Svarfd%C5%93la_saga":"https://en.wikipedia.org/wiki/Svarfd%C3%A6la_saga",
		"https://en.wikipedia.org/wiki/Vatnsd%C5%93la_saga":"https://en.wikipedia.org/wiki/Vatnsd%C3%A6la_saga",
		"https://en.wikipedia.org/wiki/%C3%96lkofra_saga":"https://en.wikipedia.org/wiki/%C3%96lkofra_%C3%BE%C3%A1ttr",
		"https://en.wikipedia.org/wiki/Delling":"https://en.wikipedia.org/wiki/Dellingr",
		"https://en.wikipedia.org/wiki/Gefjun":"https://en.wikipedia.org/wiki/Gefjon",
		"https://en.wikipedia.org/wiki/Magni":"https://en.wikipedia.org/wiki/M%C3%B3%C3%B0i_and_Magni",
		"https://en.wikipedia.org/wiki/Saga_(mythology)":"https://en.wikipedia.org/wiki/S%C3%A1ga_and_S%C3%B6kkvabekkr",
		"https://en.wikipedia.org/wiki/Thruer":"https://en.wikipedia.org/wiki/%C3%9Er%C3%BA%C3%B0r",
		"https://en.wikipedia.org/wiki/Tyr":"https://en.wikipedia.org/wiki/T%C3%BDr",
		"https://en.wikipedia.org/wiki/V%C3%A1li_(son_of_Odin)":"https://en.wikipedia.org/wiki/V%C3%A1li",
		"https://en.wikipedia.org/wiki/Var_(goddess)":"https://en.wikipedia.org/wiki/V%C3%A1r"
	}

	this.randomImages = ["2a1f816b661fee90773f64515a355262","3c76f265c1126e916f857d3253a78369",
		"5ae644f3f164b8f5cfa3b6af7f715dc8","4fbde9ddb0c8b527561334e74f2df6bf",
		"8ac3a4fe0571ae9368006d7d4f7ea85c","8f182fe0b74fc76965fc59d5a96cc8c5",
		"943fa366bae165db0695c0ef4242aa2b","a3e4f049194814a810766e152a362b3c",
		"a78f97fa8af71b067f5ab6a9cc0adab3","ba9878c3841e4fdcc12565675568c9b4",
		"ca6cc67d8f67908ebaca8764526e535c","d6e2d3c7fa134575d488eef487f185fb",
		"eaa30106524060a56384160d4894667f","f35755b4f1bc86869da8ee50f2128c48",
		"fc610750cdc889258425217c3fe97a45","fe6f220d870d5bd3acf816026bbaaa26",
		"ffc362e2bcb5f44cedef7072680eb727","7774694af8094bbb105a2f309d33765e",
		"880729b18a3ebaaa26e2eeaa52921e73","30694c483e56305bb84416c568dcd212",
		"27ded7266de83dfa45e156cc84ff8717", "5e4f68bbbadf439261bd05e8b9e2c76e"];
	
}