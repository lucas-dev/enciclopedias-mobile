function JSONToHTMLConverter(json) {
	// TODO donde ordenamos esto?
	var html = "";

	this.getHTML = function(pages) {
		for(var i=0; i<json.components.length;i++) {
			if (i===0) {
				// abriendo el container
				html += '<div class="ld__lucas">';
				html += '<div class="ld__header__options">\
							<div>\
								<span class="md-chevron-left"></span>\
							</div>\
							<div class="ld__header__options__title">'+json.components[i].content.title+'</div>\
							<div>\
								<span class="md-star-outline fav-icon"></span>\
								<span class="md-storage"></span>\
								<span class="md-home"></span>\
							</div>\
						</div>';
			}
			if (i===1) {
				html += '<div class="ld__body">'+this.getHtmlFromJSON(json.components[i], pages);
			} else if (i===json.components.length-1) {
				html += this.getHtmlFromJSON(json.components[i], pages)+'</div>'; // cerrando body
				html += '</div>'; // cerrando el container
			} else {
				// agregando elementos dentro del container
				html += this.getHtmlFromJSON(json.components[i], pages);
			}
		}
		return html;
	}

}



JSONToHTMLConverter.prototype.getHtmlFromJSON = function(obj, pages) {
	var arrayParams = [];
	// EL ORDEN DE LOS PARAMETROS QUE SE CARGAN EN EL ARRAY ESTA DADO
	// POR EL ORDEN EN QUE SE CARGAN LOS KEY EN RENDERCOMPONENT.JS 
	for (var key in obj.content) {
		var sanitizedHTML = new SanitizeHTML(obj.content[key]).sanitize(pages);
		arrayParams.push(sanitizedHTML);
	}
	return this.Functions[obj.type].apply(this, arrayParams);
}

JSONToHTMLConverter.prototype.Functions = {
	header: function(title) {
		return '<div class="ld__header">\
					<img class="ld__header__img">\
					<div class="ld__header__title">'+title+'</div>\
				</div>';
	},
	intro: function(text) {
		return '<div class="ld__intro">\
					<p><span class="capitalize">'+text.charAt(0)+'</span>\
					'+text.substring(1, text.length-1)+'</p>\
				</div>';
	},
	introStart: function(text) {
		return '<div class="ld__intro">\
					<p><span class="capitalize">'+text.charAt(0)+'</span>\
					'+text.substring(1, text.length-1)+'</p>';
	},
	introEnd: function() {
		return '</div>';
	},
	accordionStart: function(title) {
		return '<div class="ld__accordion accordion">\
					<div class="ld__accordion__header accordion__header">\
						<div class="ld__accordion__header__title">'+title+'</div>\
						<div class="ld__accordion__header__expand md-expand-less"></div>\
					</div>\
					<div class="ld__accordion__content accordion__content">';
	},
	accordionEnd: function() {
		return '<div class="ld__accordion__footer accordion__footer accordion__close">\
					<div class="ld__accordion__footer__text">Show less</div>\
					<div class="ld__accordion__footer__expand md-expand-less"></div>\
				</div>\
				</div>\
				</div>';
	},
	citation: function(content, footer) {
		return '<div class="ld__citation">\
					<div class="ld__citation__border"></div>\
					<div class="ld__citation__content">'+content+'</div>\
					<div class="ld__citation__footer">'+footer+'</div>\
				</div>';
	},
	imgText: function(img, text, position) {
		var imgElement = document.createElement("div");
		imgElement.innerHTML = this.Functions.imgCard1(img);
		imgElement = imgElement.firstChild;
		imgElement.classList.add((position==="right"?"float-right-img":"float-left-img"));
		return '<div class="ld__text-img-block">\
					'+(imgElement.outerHTML)+'\
					<p>'+text+'</p>\
				</div>';
	},
	imgCard1: function(img) {
		img = img.split("@@@");
		return '<div class="ld__card_img--1" data-url="'+img[0]+'">\
					<img>\
					<div class="ld__card_img--1__text">'+img[1]+'</div>\
				</div>';
	},
	imgCard2: function(img, text) {
		img = img.split("@@@");
		return '<div class="ld__card_img--2" data-url="'+img[0]+'">\
					<img class="ld__card_img--2__img">\
					<div class="ld__card_img--2__text">'+img[1]+'</div>\
				</div>';
	},
	title1: function(title) {
		return '<div class="ld__title--1">'+title+'</div>';
	},
	title2: function(title) {
		return '<div class="ld__title--2">'+title+'</div>';
	},
	title3: function(title) {
		return '<div class="ld__title--3">'+title+'</div>';
	},
	list1: function(li) {
		// agregar li[1]
		li = li.split("\n");
		var html = '<ul class="ld__list--1">';
		for (var i=0; i<li.length; i++) {
			var liElement = li[i].split("@@@");
			if (liElement[1]) {
				html += '<li data-link="'+liElement[0]+'">\
							<div class="list-with-content">'+liElement[1]+'</div>\
							<div class="list-with-content__icon md-chevron-right"></div>\
						</li>';
			} else {
				html += '<li>'+liElement[0]+'</li>'
			}
		}
		html += '</ul>'
		return html;
	},
	list2: function(li) {
		// agregar li[2]
		li = li.split("\n");
		var html = '<ul class="ld__list--2">';
		
		for (var i=0; i<li.length; i++) {
			var liElement = li[i].split("@@@");
			if (liElement[2]) {
				html += '<li data-link="'+liElement[0]+'">\
							<div class="ld__list--2__title">'+liElement[1]+'</div>\
							<div class="ld__list--2__content">\
								<div class="list-with-content">'+liElement[2]+'</div>\
								<div class="list-with-content__icon md-chevron-right"></div>\
							</div>\
						</li>';	
			} else {
				html += '<li>\
							<div class="ld__list--2__title">'+liElement[0]+'</div>\
							<div class="ld__list--2__content">'+liElement[1]+'</div>\
						</li>';
			}

		}
		html += '</ul>';
		return html;
	},
	list3Start: function() {
		return '<div class="ld__list--3 list-popup">';
	},
	list3End: function() {
		return '</div>';
	},
	list3LI: function(title) {
		return '<div class="ld__list--3__component list-popup__component"><div class="ld__list--3__component__header list-popup__component__header">'+title+'</div></div>';
	},
	list3LIStart: function(title) {
		return '<div class="ld__list--3__component list-popup__component">\
					<div class="ld__list--3__component__header list-popup__component__header">\
						<div>'+title+'</div>\
						<span class="md-expand-more"></span>\
					</div>\
					<div class="ld__list--3__component__content list-popup__component__content">';
	},
	list3LIEnd: function() {
		return '</div></div>';
	},
	table1: function(head, body) {
		head = head.split("@@@");
		body = body.split("\n");
		var html = '<div class="ld__table--1">\
			<div class="ld__table--1__head">';

		for (var i=0; i<head.length; i++) {
			html += '<div>'+head[i]+'</div>';
		}
		html += "</div>";
		
		html += '<div class="ld__table--1__body">';

		for (var i=0; i<body.length; i++) {
			html += '<div class="ld__table--1__body__row">';
			var column = body[i].split("@@@");
			for (var j=0; j<column.length; j++) {
				html += '<div>'+column[j]+'</div>';
			}
			html += "</div>"
		}

		html += "</div>"
		
		html += "</div>";

		return html;
	},
	gallery1: function(images) {
		images = images.split("\n");
		var html = '<div class="ld-hcarousel">\
						<div class="ld-hcarousel__wrapper">';

		for (var i=0; i<images.length; i++) {
			var image = images[i].split("@@@");
			html += '<div class="ld-hcarousel__wrapper__card" data-url="'+image[0]+'">';
			html += '<img class="ld-hcarousel__wrapper__card__img">';
			html += '<div class="ld-hcarousel__wrapper__card__text">'+image[1]+'</div>';
			html += '</div>';
			
		}
		html += "</div></div>";
		
		return html;		
	},
	gallery2: function(images) {
		images = images.split("\n");
		var html = '<div class="ld__gallery--2">';
		for (var i=0; i<images.length; i++) {
			var image = images[i].split("@@@");
			html += '<div class="ld__gallery--2__card" data-url="'+image[0]+'">';
			html += '<img class="ld__gallery--2__card__img">';
			html += '<div class="ld__gallery--2__card__text">'+image[1]+'</div>';
			html += '</div>';
			
		}
		html += "</div>";
		
		return html;		
	},
	gallery3: function(images) {
		images = images.split("\n");
		var html = '<div class="ld__gallery--3">';
		for (var i=0; i<images.length; i++) {
			var image = images[i].split("@@@");
			html += '<div class="ld__gallery--3__card" data-url="'+image[0]+'">';
			html += '<img class="ld__gallery--3__card__img">';
			html += '<div class="ld__gallery--3__card__text">'+image[1]+'</div>';
			html += '</div>';
			
		}
		html += "</div>";
		
		return html;		
	},
	column2: function(column) {
		column = column.split("@@@");
		var html = '<div class="ld__column--2">';
		for (var i=0; i<column.length; i++) {
			html += '<div>'+column[i]+'</div>';
		}
		html += "</div>";
		return html;
	},
	column3: function(column) {
		column = column.split("@@@");
		var html = '<div class="ld__column--3">';
		for (var i=0; i<column.length; i++) {
			html += '<div>'+column[i]+'</div>';
		}
		html += "</div>";
		return html;
	},
	text: function(html) {
		return html;
	},
	freeScrollStart: function() {
		return '<div class="ld__free-scroll">';
	},
	freeScrollEnd: function() {
		return '</div>';
	}
}

