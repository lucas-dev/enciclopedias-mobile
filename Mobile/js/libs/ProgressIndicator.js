function ProgressIndicator() {
	this.indicatorContainer = document.createElement("div");
	this.indicatorContainer.classList.add("progress");
	this.indicatorContainer.style.top = 0;	

	this.indicatorContent = document.createElement("div");
	this.indicatorContent.classList.add("indeterminate");

	this.indicatorContainer.appendChild(this.indicatorContent);

	document.body.appendChild(this.indicatorContainer);
	this.indicatorContainer.style.visibility = "none";

	this.start = function() {
		this.indicatorContainer.style.visibility = "visible";

	} 

	this.stop = function() {
		this.indicatorContainer.style.visibility = "hidden";
	}
}