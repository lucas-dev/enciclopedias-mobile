function SimpleSideMenu(menuElement, edgeDirection, animate, animationTime) {
	var that = this;
	this.hammer = null;

	this.animationTime = animationTime?animationTime:0.5;
	this.edgeDirection = edgeDirection;
	this.menuElement = menuElement;
	this.coord = {close:-100,open: 0};
	this.menuTransition = {
		enable: function() {
			that.menuElement.style["-webkit-transition"] = "-webkit-transform "+ that.animationTime +"s";
		},
		disable: function() {
			that.menuElement.style["-webkit-transition"] = "none";
		}
	}
	this.handleVisibility =
	{
		close: function() {
			menuElement.style["-webkit-transform"] = "translate3d("+that.coord.close+"%,0,0)";
		},
		open: function() {
			menuElement.style["-webkit-transform"] = "translate3d("+that.coord.open+"%,0,0)";
		}
	}

	this.wrapper = document.createElement("div");
	this.wrapper.className = "slide-wrapper";
	this.wrapper.appendChild(menuElement);

	this.menuTransition.enable();
	if (edgeDirection === "right") {
		menuElement.style.right = 0;
		this.coord.close = 100;
		this.coord.open = 0;
	}

	this.handleVisibility.close();

	this.open = function() {
		this.hammer = new Hammer(this.wrapper, {touchAction: "pan-y"});

		this.hammer.on("panstart", function(evt) {
			that.menuTransition.disable();
		});

		this.hammer.on("panleft panright", function(evt) {
			var distanceToMovePercent = ((evt.deltaX/window.innerWidth)*100);
			var condition = (that.edgeDirection==="right" && distanceToMovePercent >= that.coord.open)||
	        (that.edgeDirection==="left" && distanceToMovePercent <= that.coord.open);
	        if (condition) {
				that.menuElement.style["-webkit-transform"] = "translate3d("+distanceToMovePercent+"%,0,0)";
	        }
		});

		this.hammer.on("panend tap", function(evt) {
			that.close();
		});


		document.body.appendChild(this.wrapper);
		//this.slide().attachListeners();
		if (animate) {
			setTimeout(function() {
				that.handleVisibility.open();
			}, 100)
		} else {
			that.handleVisibility.open();
		}
	}

	this.close = function() {
		this.hammer = null;
		this.menuTransition.enable();
		this.handleVisibility.close();
		setTimeout(function() {
			if (document.body.contains(that.wrapper)) {
				document.body.removeChild(that.wrapper);
			}
		}, that.animationTime*1000);
	    //this.slide().detachListeners();
	}

	this.isMenuActive = function() {
		return document.body.contains(this.wrapper);
	}

}
