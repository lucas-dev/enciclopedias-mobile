function SearchPlugin(wikiObj) {
	var that = this;
	this.scrollY = 0;
	this.resultPages = [];
	this.resultGeneral = [];

	this.searchCallback = null;

	this.innerHTML = "";
	this.init = function() {
		that.ux.reloadDOM();
		that.dom.searchBtn.onclick = function() {
			that.utils.clear();
			wikiObj.progressIndicator.start();
			that.utils.findInJSON(that.dom.inputText.value);
			wikiObj.progressIndicator.stop();
			that.ux.setMessageArea(that.dom.inputText.value);
			that.ux.populateResultArea();
			that.ux.setUpDataLink();

			that.innerHTML = that.dom.page.innerHTML;
		}
	}

	this.ux = {
		getDOM: function() {
			return '<div class="ld__search-screen">\
						<div class="ld__header__options">\
							<div>\
								<span class="md-chevron-left"></span>\
							</div>\
							<div class="ld__header__options__title">Search in the Encyclopedia</div>\
						</div>\
						<div class="ld__header">\
							<img class="ld__header__img">\
							<div class="ld__header__title">Search in the Encyclopedia</div>\
						</div>\
						<div class="ld__search-screen__search-area">\
					          <input type="text" class="ld__search-screen__search-area__input" placeholder="Enter search term" />\
					          <input type="submit" class="ld__search-screen__search-area__btn" value="" />\
						</div>\
						<div class="ld__search-screen__message-area"></div>\
						<div class="ld__search-screen__results-area"></div>\
					</div>';
		},
		makeTitle: function(title) {
			var div = document.createElement("div");
			div.innerHTML = '<div class="ld__search-screen__results-area__title">'+title+'</div>';
			return div.firstChild;
		},
		makeRow: function(text, badge, link) {
			var div = document.createElement("div");
			div.innerHTML = '<div class="ld__search-screen__results-area__row" data-link="'+link+'">\
						<div class="ld__search-screen__results-area__row--content">'+text+'</div>\
						<div class="ld__search-screen__results-area__row--badge"><span>'+badge+'</span></div>\
						<div class="ld__search-screen__results-area__row--icon md-chevron-right"></div>\
					</div>';
			return div.firstChild;
		},
		populateResultArea: function() {

			if (that.resultPages.length) {
				that.dom.resultArea.appendChild(that.ux.makeTitle("Pages"));
				for (var i=0; i<that.resultPages.length; i++) {
					that.dom.resultArea.appendChild(that.ux.makeRow(decodeURIComponent(that.resultPages[i].title),"", that.resultPages[i].url));
				}
			}

			if (that.resultGeneral.length) {
				that.dom.resultArea.appendChild(that.ux.makeTitle("Pages containing the search term"));
				for (var i=0; i<that.resultGeneral.length; i++) {
					that.dom.resultArea.appendChild(that.ux.makeRow(decodeURIComponent(that.resultGeneral[i].title), that.resultGeneral[i].occurrences, that.resultGeneral[i].url));
				}
			}

		},
		setMessageArea: function(searchTerm) {
			that.dom.messageArea.innerHTML = "<b>"+(that.resultPages.length + that.resultGeneral.length)+'</b> results found for the \
			search term <b>\"'+searchTerm+"\"</b>";
		},
		reloadDOM: function() {
			that.dom.page = document.querySelector(".ld__search-screen");
			that.dom.inputText = that.dom.page.querySelector(".ld__search-screen__search-area__input");
			that.dom.searchBtn = that.dom.page.querySelector(".ld__search-screen__search-area__btn");
			that.dom.resultArea = that.dom.page.querySelector(".ld__search-screen__results-area");
			that.dom.messageArea = that.dom.page.querySelector(".ld__search-screen__message-area");
		},
		setUpDataLink: function() {
			var dataLink = that.dom.page.querySelectorAll("[data-link]");
			if (dataLink) {
				for (var i=0; i<dataLink.length; i++) {
					(function(i) {
						dataLink[i].addEventListener("click", function() {
							that.searchCallback(dataLink[i].dataset.link);
							that.scrollY = window.scrollY;
						});
					})(i);
				}
			}
		},
		reloadBack: function() {
			that.ux.reloadDOM();
			that.dom.page.innerHTML = that.innerHTML;
			that.init();
			that.ux.setUpDataLink();
			window.scrollTo(0, that.scrollY);
		}
	}

	this.utils = {
		getOccurrences: function(string, subString, allowOverlapping){

		    string+=""; subString+="";
		    if(subString.length<=0) return string.length+1;

		    var n=0, pos=0;
		    var step=allowOverlapping?1:subString.length;

		    while(true){
		        pos=string.indexOf(subString,pos);
		        if(pos>=0){ ++n; pos+=step; } else break;
		    }
		    return n;
		},

		findInJSON: function(searchTerm) {
			var occurrences = 0;
			for (var key in wiki.pages) {
				occurrences = that.utils.getOccurrences(key.toLowerCase(), searchTerm.toLowerCase());
				if (occurrences) {
					that.resultPages.push({
						title: key.split("/").pop(),
						url: key,
						occurrences: occurrences
					});
				}

				occurrences = that.utils.getOccurrences(JSON.stringify(wiki.pages[key].components).toLowerCase(), 
					searchTerm.toLowerCase());
				if (occurrences) {
					that.resultGeneral.push({
						title: key.split("/").pop(),
						url: key,
						occurrences: occurrences
					});	
				}
			}
		},
		clear: function() {
			that.dom.resultArea.innerHTML = "";
			that.resultPages = [];
			that.resultGeneral = [];
			this.innerHTML = "";

		}
	}

	this.dom = {
		searchBtn: null,
		inputText: null,
		resultArea: null,
		messageArea: null
	}
	
}