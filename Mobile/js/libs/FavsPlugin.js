function FavsPlugin(wikiObj) {
	var that = this;

	this.favsCallback = null;
	this.dom = {
		favsScreen: null,
		resultsArea: null
	}

	this.ux = {
		getDOM: function() {
			return '<div class="ld__favs-screen">\
						<div class="ld__header__options">\
							<div>\
								<span class="md-chevron-left"></span>\
							</div>\
							<div class="ld__header__options__title">Favorite Pages</div>\
						</div>\
						<div class="ld__header">\
							<img class="ld__header__img">\
							<div class="ld__header__title">Favorite Pages</div>\
						</div>\
						<div class="ld__favs-screen__results-area"></div>\
					</div>';
		},
		getFavRow: function(link, text) {
			var div = document.createElement("div");
			div.innerHTML = '<div class="ld__favs-screen__results-area__row" data-link="'+link+'">\
					<div class="ld__favs-screen__results-area__row--content">'+text+'</div>\
					<div class="ld__favs-screen__results-area__row--icon md-chevron-right"></div>\
				</div>';
			return div;
		},
		populateFavsArea: function() {
			var favs = that.utils.getFavsList();
			if (favs && favs.length) {
				for (var i=0; i<favs.length;i++) {
					that.dom.resultsArea.appendChild(that.ux.getFavRow(favs[i],decodeURIComponent(favs[i].split("/").pop())));
				}
			}
		},
		reloadDOM: function() {
			that.dom.favsScreen = document.querySelector(".ld__favs-screen");
			that.dom.resultsArea = that.dom.favsScreen.querySelector(".ld__favs-screen__results-area");
		},
		setUpDataLink: function() {
			var dataLink = that.dom.favsScreen.querySelectorAll("[data-link]");
			if (dataLink) {
				for (var i=0; i<dataLink.length; i++) {
					(function(i) {
						dataLink[i].addEventListener("click", function() {
							that.favsCallback(dataLink[i].dataset.link);
						});
					})(i);
				}
			}
		}
	}

	this.persistence = {
		savePage: function(page) {
			var obj = that.utils.getFavsList();
			if (obj) {
				// chequeo que no sea repetido el valor
				if (obj.indexOf(page)<=-1)
					obj.push(page);	
			} else {
				obj = [page];
			}
			console.log("savePage: "+obj);
			localStorage.setItem(wikiObj.hcStuff.GLOBAL.LOCAL_STORAGE_KEY, JSON.stringify(obj));
		},
		removePage: function(page) {
			var obj = that.utils.getFavsList();

			// eliminando el item con el nombre de la pagina
			var index = obj.indexOf(page);
			obj.splice(index, 1);

			console.log("removePage: "+obj);
			localStorage.setItem(wikiObj.hcStuff.GLOBAL.LOCAL_STORAGE_KEY, JSON.stringify(obj));
		}
	}

	this.utils = {
		isPageInFavsList: function(page) {
			var list = that.utils.getFavsList();
			if ((list && list.indexOf(page) > -1)) 
				return true;
			return false;
		},
		getFavsList: function() {
			return JSON.parse(localStorage.getItem(wikiObj.hcStuff.GLOBAL.LOCAL_STORAGE_KEY));
		}
	}
}