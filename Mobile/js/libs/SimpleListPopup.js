function SimpleListPopup(container, closeLastActive, callback) {
	var lastActive=null;
	var components = container.children;
	
	var collapseComponent = function(i) {
		components[i].classList.remove("list-popup--active");
		if (components[i].children[1])
			components[i].children[1].style.display = "none";
		if (callback) callback(i);
	}

	var expandComponent = function(i) {
		components[i].classList.add("list-popup--active");
		if (components[i].children[1])
			components[i].children[1].style.display = "block";
		if (callback) callback(i);
	}
	for (var i=0; i<components.length; i++) {
		if (components[i].children[1])
			components[i].children[1].style.display = "none";
		(function(i) {
			// click en el header
			components[i].children[0].onclick = function() {
				if (closeLastActive && lastActive!=null && lastActive!==i) {
					collapseComponent(lastActive);
				}

				
				if (components[i].classList.contains("list-popup--active")) {
					collapseComponent(i);
					lastActive = null;
				} else {	
					expandComponent(i);
					lastActive = i;
				}
				
			}
		})(i);
	}
}