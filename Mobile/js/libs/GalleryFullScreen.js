/*
	TODO
		- activity indicator para la carga de imagenes	
		- callback en loadSlider del onFlipEnd

*/
function GalleryFullScreen(elements, buttonsCallback) {
	var that = this;

	this.buttonsCallback = buttonsCallback;
	this.elements = elements;
	this.index = 0;

	this.ui = new this.UI(this);
	this.ux = new this.UX(this);
	this.utils = new this.Utils(this);

	this.pinchZoom = new this.PinchZoom(that.ui.imgCenter);

	this.scroller = new this.Scroll(
		{
			container: that.ui.imgWrapper,
			mouseMoveCallback:  function(scroll) {
									if (that.ui.imgScroller.getBoundingClientRect().top === 0) {
										that.ui.imageArea.style["-webkit-transform"] = "translate3d("+(scroll.distanceToMovePercent)+"%,0,0)";
									}
								},
			mouseUpCallback: function(scroll) {
								if (that.ui.imgScroller.getBoundingClientRect().top === 0) {
									if (scroll.distanceToMovePercent) {
										that.ui.imageArea.style["-webkit-transform"] = "translate3d(0,0,0)";
										if (Math.abs(scroll.distanceToMovePercent)>20) {
											var index = 0;
											if (scroll.distanceToMovePercent < 0) {
												index = that.utils.resolveIndex(that.index+1);
											} else {
												index = that.utils.resolveIndex(that.index-1);
											}
											that.ux.loadSlider(index);
											that.pinchZoom.reset();
										}
									}	
								}

							},
			tapCallback: function() {
				that.ux.toogleHeaderFooter();
			}
		}

	);


	this.open = this.ux.open;
	this.close = this.ux.close;

	
	this.destroy = function() {

	}

	this.ui.positionImages();
	this.ux.activateListeners();
}

GalleryFullScreen.prototype.UX = function(that) {	
	this.activateListeners = function() {
		that.ui.closeBtn.onclick = function() {
			that.ux.close();
		}

		that.ui.imageArea.onclick = function() {
			
		}

		that.ui.infoBtn.onclick = function() {
			that.buttonsCallback.info(that.index);
		}

		that.ui.imageViewer.onclick = function() {
			that.buttonsCallback.imageViewer(that.index);
		}

		that.ui.share.onclick = function() {
			that.buttonsCallback.share(that.index);
		}
	}

	this.open = function(index) {
		that.ui.body.appendChild(that.ui.container);
		that.ui.container.classList.add("fade-in");

		// si no se recibe el index, que sea 0
		that.ux.loadSlider(index?index:0);
	}
	
	this.isGalleryActive = function() {
		return that.ui.body.contains(that.ui.container);
	}

	this.close = function() {
		// TODO remover los eventListeners del touch handler
		that.ui.container.classList.add("fade-out");
		
		
		setTimeout(function() {
			that.ui.container.classList.remove("fade-out");
			that.ui.body.removeChild(that.ui.container);
		}, 800);
	}

	this.loadSlider = function(index) {
		that.index = index;

		that.ui.imgLeft.src = that.elements[that.utils.resolveIndex(index-1)].image;
		that.ui.imgCenter.src = that.elements[index].image;
		that.ui.imgRight.src = that.elements[that.utils.resolveIndex(index+1)].image;

		that.ui.title.innerHTML = that.elements[index].title;
		that.ui.description1.innerHTML = that.elements[index].description1;
		that.ui.description2.innerHTML = that.elements[index].description2;

		// eliminando posibles href
		var hrefs = that.ui.container.querySelectorAll("a");
		for (var i=0; i<hrefs.length; i++) {
			hrefs[i].removeAttribute("href");
		}

	}

	this.toogleHeaderFooter = function() {
		if (that.ui.optionsArea.getBoundingClientRect().top===0) {
			that.ui.optionsArea.style["-webkit-transform"] = "translate3d(0, -100%, 0)";
			that.ui.detailsArea.style["-webkit-transform"] = "translate3d(0, 100%, 0)";
		} else {
			that.ui.optionsArea.style["-webkit-transform"] = "translate3d(0, 0%, 0)";
			that.ui.detailsArea.style["-webkit-transform"] = "translate3d(0, 0%, 0)";
		}
	}

	this.onFlipEnd = function() {

	}
}

GalleryFullScreen.prototype.UI = function() {
	this.body = document.body;
	
	this.container = document.createElement("div");
	this.container.innerHTML = '<div class="ld__img-full-screen">\
								    <div class="ld__img-full-screen__image">\
										<img id="img-left">\
										<div class="ld__img-full-screen__image__wrapper">\
							                <div class="ld__img-full-screen__image__wrapper__scroller">\
							                    <img id="img-center">\
							                </div>\
							            </div>\
										<img id="img-right">\
								    </div>\
								    <div class="ld__img-full-screen__options">\
								        <div>\
								            <span class="md-close"></span>\
								        </div>\
								        <div>\
								            <span class="md-info"></span>\
								            <span class="md-file-download"></span>\
								            <span class="md-share"></span>\
								        </div>\
								    </div>\
								    <div class="ld__img-full-screen__details">\
								        <div class="ld__img-full-screen__details__title">Title</div>\
								        <div class="ld__img-full-screen__details__description--1">description 1</div>\
								        <div class="ld__img-full-screen__details__description--2">description 2</div>\
								    </div>\
								</div>';
	this.container = this.container.firstChild;


	this.detailsArea = this.container.querySelector(".ld__img-full-screen__details");
	this.title = this.detailsArea.querySelector(".ld__img-full-screen__details__title");
	this.description1 = this.detailsArea.querySelector(".ld__img-full-screen__details__description--1");
	this.description2 = this.detailsArea.querySelector(".ld__img-full-screen__details__description--2");
	
	
	this.optionsArea = this.container.querySelector(".ld__img-full-screen__options");
	this.closeBtn = this.optionsArea.querySelector(".md-close");
	this.infoBtn = this.optionsArea.querySelector(".md-info");
	this.imageViewer = this.optionsArea.querySelector(".md-file-download");
	this.share = this.optionsArea.querySelector(".md-share");

	this.imageArea = this.container.querySelector(".ld__img-full-screen__image");
	this.imgLeft = this.imageArea.querySelector("#img-left");
	this.imgCenter = this.imageArea.querySelector("#img-center");
	this.imgRight = this.imageArea.querySelector("#img-right");

	this.imgWrapper = this.imageArea.querySelector(".ld__img-full-screen__image__wrapper");
	this.imgScroller = this.imgWrapper.querySelector(".ld__img-full-screen__image__wrapper__scroller");

	this.positionImages = function() {
		this.imgLeft.style.left = "-"+window.innerWidth+"px";
		this.imgRight.onload = function() {
			this.style.left = this.offsetWidth+"px";
		}
	}

}


GalleryFullScreen.prototype.Scroll = function(params) {
	var thiz = this;
	var hammer = new Hammer(params.container);

	this.distanceToMovePercent = 0;

	hammer.on("panstart", function(evt) {
		
	});

	hammer.on("panleft panright", function(evt) {
		thiz.distanceToMovePercent = ((evt.deltaX/window.innerWidth)*100);
		params.mouseMoveCallback(thiz);
	});

	hammer.on("panend", function(evt) {
		params.mouseUpCallback(thiz);
	});

	hammer.on("tap", function(evt) {
		params.tapCallback();
	});
}

GalleryFullScreen.prototype.PinchZoom = function(elm) {
	hammertime = new Hammer(elm, {});
	hammertime.get('pinch').set({
	    enable: true
	});
	var posX = 0,
	    posY = 0,
	    scale = 1,
	    last_scale = 1,
	    last_posX = 0,
	    last_posY = 0,
	    max_pos_x = 0,
	    max_pos_y = 0,
	    transform = "",
	    el = elm;
	
	hammertime.on('doubletap pan pinch panend pinchend', function(ev) {
	    if (ev.type == "doubletap") {
	        transform =
	            "translate3d(0, 0, 0) " +
	            "scale3d(2, 2, 1) ";
	        scale = 2;
	        last_scale = 2;
	        try {
	            if (window.getComputedStyle(el, null).getPropertyValue('-webkit-transform').toString() != "matrix(1, 0, 0, 1, 0, 0)") {
	                transform =
	                    "translate3d(0, 0, 0) " +
	                    "scale3d(1, 1, 1) ";
	                scale = 1;
	                last_scale = 1;
	            }
	        } catch (err) {}
	        el.style.webkitTransform = transform;
	        transform = "";
	    }

	    //pan    
	    if (scale != 1) {
	        posX = last_posX + ev.deltaX;
	        posY = last_posY + ev.deltaY;
	        max_pos_x = Math.ceil((scale - 1) * el.clientWidth / 2);
	        max_pos_y = Math.ceil((scale - 1) * el.clientHeight / 2);
	        if (posX > max_pos_x) {
	            posX = max_pos_x;
	        }
	        if (posX < -max_pos_x) {
	            posX = -max_pos_x;
	        }
	        if (posY > max_pos_y) {
	            posY = max_pos_y;
	        }
	        if (posY < -max_pos_y) {
	            posY = -max_pos_y;
	        }
	    }


	    //pinch
	    if (ev.type == "pinch") {
	        scale = Math.max(.999, Math.min(last_scale * (ev.scale), 4));
	    }
	    if(ev.type == "pinchend"){last_scale = scale;}

	    //panend
	    if(ev.type == "panend"){
	    last_posX = posX < max_pos_x ? posX : max_pos_x;
	    last_posY = posY < max_pos_y ? posY : max_pos_y;
	    }

	    if (scale != 1) {
	        transform =
	            "translate3d(" + posX + "px," + posY + "px, 0) " +
	            "scale3d(" + scale + ", " + scale + ", 1)";
	    }

	    if (transform) {
	        el.style.webkitTransform = transform;
	    }
	});

	this.reset = function() {
		posX = 0;
	    posY = 0;
	    scale = 1;
	    last_scale = 1;
	    last_posX = 0;
	    last_posY = 0;
	    max_pos_x = 0;
	    max_pos_y = 0;
	    transform = "";
	    el.style.webkitTransform = "none";
	}
}

GalleryFullScreen.prototype.Utils = function(that) {
	this.resolveIndex = function(index) {
		if (index>that.elements.length-1) {
			index = 0;
		} else if (index < 0) {
			index = that.elements.length-1;
		}

		return index;
	}
}