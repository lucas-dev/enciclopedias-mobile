function SimpleToast() {
	var that = this;
	var removeFromBody = false;
	
	var toast = document.createElement("div");
	toast.classList.add("ld-toast");
	
	this.callback = null;
	this.showTime = 1500;

	toast.addEventListener( 'webkitAnimationEnd',  function(event) { 
		if (removeFromBody) {
			document.body.removeChild(toast);
		} else {
			setTimeout(function() {
				that.close();
			}, that.showTime);
		}
	}, false );

	this.open = function(msg) {
		if (document.body.contains(toast))
			document.body.removeChild(toast);
		removeFromBody = false;
		toast.innerHTML = msg;
		toast.classList.remove("closeToast");
		toast.classList.add("openToast");

		document.body.appendChild(toast);
	}

	this.close = function() {
		removeFromBody = true;
		toast.classList.remove("openToast");
		toast.classList.add("closeToast");
	}
}