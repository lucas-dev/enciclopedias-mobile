import sys
import urllib
import json
from PIL import Image
import os


path_file = sys.argv[1]
path_save_into = sys.argv[2]

# process JSON
with open(path_file) as json_file:
	data = json.load(json_file)

for majorkey, subdict in data.iteritems():
	try:
	    url = data[majorkey]["url"]
	    img_name = data[majorkey]["md5"]
	    img_full_path = (path_save_into+img_name)
	    urllib.urlretrieve(url, img_full_path)
	    try:
	    	# cambiar de formato a jpg
	    	img = Image.open(img_full_path).convert('RGB').save(os.path.splitext(img_full_path)[0]+".jpg") # reemplazamos la extension original por jpg
	    	# eliminar la imagen original una vez que se hizo la conversion
	    	os.remove(img_full_path)
	    except:
	    	print "could not change extension or remove: ", img_full_path
	except:
		print "could not save: ",url

