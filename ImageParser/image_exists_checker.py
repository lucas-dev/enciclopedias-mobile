import os, sys, json, shutil


path_file = sys.argv[1]
path_img_folder = sys.argv[2]

folder_wrong_images = path_img_folder+"FIX"

# process JSON
with open(path_file) as json_file:
	data = json.load(json_file)

for majorkey, subdict in data.iteritems():
    md5 = data[majorkey]["md5"]
    path = path_img_folder+md5+".jpg"
    if not (os.path.isfile(path)):
    	if not os.path.exists(folder_wrong_images):
    		os.mkdir(folder_wrong_images)
    	shutil.move(path_img_folder+md5, folder_wrong_images+"/"+md5)
